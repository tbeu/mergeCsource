# Use make if you want to compile mergeCsource.exe on Windows/MinGW

CC = gcc

CFLAGS = -O3

OBJS = mergeCsource.o GString.o ListAll.o

mergeCsource.exe: $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) -o $@

mergeCsource.o : mergeCsource.c
	$(CC) $(CFLAGS) -c mergeCsource.c

GString.o : GString.c
	$(CC) $(CFLAGS) -c GString.c

ListAll.o : ListAll.c
	$(CC) $(CFLAGS) -c ListAll.c

clean :
	rm -f $(OBJS)
	rm -f mergeCsource.exe
