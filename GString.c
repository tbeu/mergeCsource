/*
 *  GString.c
 *
 *  Created by jcbeatty.
 *  Copyright 2009 University of Waterloo. All rights reserved.
 *
 *  The source and header files are called "GString.h/c" rather than just "String.h/c"
 *  to avoid confusion with the system header file <string.h>. The "G" is for "growable".
 *  Probably I should call the struct itself GString instead of String, but I haven't...
 *
 *  See the documentation for each routine's contract.
 */

#include "GString.h"

#define DEBUG false

//static union StringStructMagicCookie ssMagicCookie;.

// For monitoring the heap. Must *follow* stdio and stdlib.
#ifdef DMALLOC
    #include "dmallocFor241.h"
#endif

// --------------------------------------------------------------------------------------
//
//   Prototypes for helpers located at the end of the file.
//
// --------------------------------------------------------------------------------------

static   // Accessible only from other routines in this file.
void crashIfArgIsNullWithMessage( char * fileName, int lineNumber, void * ptr, char * msg );

static   // Accessible only from other routines in this file.
void expandStringIfNecessary( String * theString, int sizeNeeded, int expansionFactor );

// Added to keep the Solaris gcc compiler happy - for some reason it's not declared by <stdio.h>.
// extern int  vsnprintf( char *, size_t, const char *, va_list );

static   // Used to verify that the magic cookie with which initialized StringStructs are tagged is correct.
void crashIfMagicCookieIsBad( char * fileName, int lineNumber, StringPointer, char * msg );

// Used as the 3rd argument when calling expandStringIfNecessary(...)
// #define ScaleUpByOne 1
// #define ScaleUpByTwo 2
static const int ScaleUpByOne = 1;
static const int ScaleUpByTwo = 2;

static SSMC ssMagicCookieValue;

// --------------------------------------------------------------------------------------
//
//     Constructors.
//
// --------------------------------------------------------------------------------------

// Available space is NOT assumed to include room for a terminating '\0'. So we add +1 to provide room for it.
StringPointer _newEmptyStringWithCapacity( char * fileName, int lineNumber, int availableSpace ) {

    // Initialize the constant used to tag String structs as having been properly initialized.
    // We initialize the constant in every String allocation because it seems we can't do that directly in a global initializer (verify...).
    // It's over-optimization, but for the fun of it I use a union so that we can easily initialize the cookie with a string, but use
    // an integer comparison to verify in crashIfMagicCookieIsBad(...) that the struct has a valid cookie value.
    strcpy( ssMagicCookieValue.mcStrVal, "StringSt" );

    String * newString     = (String*) malloc( sizeof(String) );

    availableSpace         = (availableSpace < 0 ? 0 : availableSpace );              // Force availableSpace to be at least 0.
    int sizeOfBlock        = (availableSpace+1) * sizeof(char) ;                      // +1 for the terminating '\0',
    if( DEBUG ) fprintf( stderr, "newEmptyStringWithCapacity(...) is requesting %d bytes for its string field.\n", sizeOfBlock );
    newString->string      = (char*) malloc( sizeOfBlock );

    newString->magicCookie.mcIntVal = ssMagicCookieValue.mcIntVal;
    newString->string[0]   = '\0';
    newString->length      = 0;                                                       // Does not count space for the terminating '\0'
    newString->capacity    = availableSpace;                                          // Does not count space for the terminating '\0'
    newString->tabOrigin   = 0;
    return newString;
}

// Chars is assumed to be nul terminated.
StringPointer _newStringFromChars( char * fileName, int lineNumber, char * chars ) {
    crashIfArgIsNullWithMessage( fileName, lineNumber, chars, "newStringFromChars(...) called with a NULL argument." );
    String * newString  = newEmptyStringWithCapacity( strlen(chars) );
    appendCharsToString( chars, newString );
    return newString;
}

StringPointer _newStringFromString( char * fileName, int lineNumber, String * str ) {
    crashIfArgIsNullWithMessage( fileName, lineNumber, str, "newStringFromString(...) called with a NULL argument." );
    String * newString  = newEmptyStringWithCapacity( str->length );
    _appendStringToString( fileName, lineNumber, str, newString );
    return newString;
}

// --------------------------------------------------------------------------------------
//
//    Public routines.
//
// --------------------------------------------------------------------------------------

#define versionGSstrLen 100

static char gs_versionStr[versionGSstrLen];

// fileName and lineNumber are fed through for consistency - there are no user errors possible.
char * _stringVersion( char * fileName, int lineNumber ) {
    // If you change versionGSstrLen, take care that it's long enough to hold what's written into it by snprintf(...),
    // an example of which is the 64 character string "GString version 1.1 (compiled on Jul 10 2009 at 13:37:49 hours)\0".
    // The C preprocessor will replace __DATE__ and __TIME__ with appropriate literal strings.
    #ifdef DMALLOC
        char * dmallocFlag = "w/dmalloc ";
    #else
        char * dmallocFlag = "";
    #endif
    int charsWritten = snprintf( gs_versionStr, versionGSstrLen, "GString version 2.1 %s(compiled on %s at %s hours)", dmallocFlag, __DATE__, __TIME__ );
    if( charsWritten >= versionGSstrLen ) {
        fprintf( stderr, "Warning: stringVersion's returned value was too large for its static buffer, so it was truncated.\n" );
    }
    return gs_versionStr;
}

void _setTabOriginForString( char * fileName, int lineNumber, String * theString ) {
    crashIfMagicCookieIsBad( fileName, lineNumber, theString, "ERROR: setTabOriginForString(...) called with a faulty String pointer." );
    theString->tabOrigin = theString->length;
    return;
}

#undef versionGSstrLen

// tabTo is the index in theString->string at which the next char will be deposited
// by the first following call of a String routine. If we've already passed the
// requested tab position the call has no effect.
void _tabToPosInString( char * fileName, int lineNumber, int tabTo, String * theString ) {
    crashIfArgIsNullWithMessage( fileName, lineNumber, theString, "tabToPosInString(...) called with a NULL String argument."  );
    crashIfMagicCookieIsBad(     fileName, lineNumber, theString, "tabToPosInString(...) called with a faulty String pointer." );
    int targetPosition = theString->tabOrigin + tabTo;
    expandStringIfNecessary( theString, targetPosition, ScaleUpByTwo );
    while( theString->length < targetPosition )
        theString->string[ theString->length++ ] = ' ';
    return;
}

void _appendCharsToString( char * fileName, int lineNumber, char * charsToAppend, String * theString )
{
    crashIfArgIsNullWithMessage( fileName, lineNumber, charsToAppend, "appendCharsToString(...) called with a NULL \"charsToAppend\" argument." );
    crashIfArgIsNullWithMessage( fileName, lineNumber, theString,     "appendCharsToString(...) called with a NULL String argument."            );
    crashIfMagicCookieIsBad(     fileName, lineNumber, theString,     "appendCharsToString(...) called with a faulty String pointer."           );

    // Neither length includes the terminating '\0'.
    int numberOfCharsToAppend = strlen( charsToAppend );
    int combinedLength        = theString->length + numberOfCharsToAppend;

    expandStringIfNecessary( theString, combinedLength, ScaleUpByTwo );

    // Append chars to theString->string.
    char * des = theString->string + theString->length;   // Pointer to the null in theString (ie of the 1st char following current valid data in String).
    char * src = charsToAppend;                           // Pointer to the first char of elt.  We assume that elt is terminated by a '\0'.
    while( (*des++ = *src++) );                           // NB: "*pt1++ = *ptr2++" is an assignment. Loop is terminated by the '\0' at the end of elt.
    // The null character '\0' assumed to terminate charsToAppend gets copied on the last iteration.

    theString->length += numberOfCharsToAppend;

    return;
}

void _appendStringToString( char * fileName, int lineNumber, String * stringToAppend, String * theString )
{
    crashIfArgIsNullWithMessage( fileName, lineNumber, stringToAppend, "the first argument to appendStringToString(...) (the String to append) is NULL."                        );
    crashIfArgIsNullWithMessage( fileName, lineNumber, theString,      "the second argument to appendStringToString(...) (the String to which text is to be appended) is NULL." );
    crashIfMagicCookieIsBad(     fileName, lineNumber, theString,      "appendStringToString(...) called with a faulty String pointer."                                         );

    // Neither length includes the terminating '\0'.
    int numberOfCharsToAppend = stringToAppend->length;
    int combinedLength        = theString->length + numberOfCharsToAppend;

    expandStringIfNecessary( theString, combinedLength, ScaleUpByTwo );

    // Append chars to theString->string.
    char * src = stringToAppend->string;                 // Pointer to the first char of stringToAppend.  We assume it is terminated by a '\0'.
    char * des = theString->string + theString->length;  // Pointer to the nul in theString (ie of the 1st char following current valid data in theString->string[]).
    while( (*des++ = *src++) );                          // NB: "*pt1++ = *src++" is an assignment. The loop oop is terminated by the '\0' at the end of stringToAppend->string.
    // The null character '\0' assumed to terminate stringToAppend->string gets copied on the last iteration.

    theString->length += numberOfCharsToAppend;

    return;
}

void _appendCharToString( char * fileName, int lineNumber, char charToAppend, String * theString )
{
    crashIfArgIsNullWithMessage( fileName, lineNumber, theString, "appendCharToString(...) called with a NULL String argument."  );
    crashIfMagicCookieIsBad(     fileName, lineNumber, theString, "appendCharToString(...) called with a faulty String pointer." );

    // Does not count space for the terminating '\0', which expandStringIfNecessary will take care of.
    int combinedLength = theString->length + 1;

    expandStringIfNecessary( theString, combinedLength, ScaleUpByTwo );

    // Append char to theString->string.
    theString->string[ theString->length     ] = charToAppend;
    theString->string[ theString->length + 1 ] = '\0';

    theString->length++;

    return;
}

// This function passes the variable number of arguments on to snprintf using the va_list / va_start / va_end macros
// described, for example, in Kernighan and Ritchie's "The C Programming Language (2/e)",  pp 155-156 & 173-174.
void _appendToStringFromFormat( char * fileName, int lineNumber, String * theString, char * format,  ... )
{
    crashIfArgIsNullWithMessage( fileName, lineNumber, theString, "the String argument to appendToStringFromFormat(...) is NULL."      );
    crashIfArgIsNullWithMessage( fileName, lineNumber, format,    "the format argument to appendToStringFromFormat(...) is NULL."      );
    crashIfMagicCookieIsBad(     fileName, lineNumber, theString, "appendToStringFromFormat(...) called with a faulty String pointer." );

    int     freeSpaceInString = theString->capacity - theString->length;
    va_list args;

    va_start( args, format );
    // The remaining space in the buffer is one larger because the string has room for the terminating '\0',
    // which is not included in theString -> capacity. And then we test charsNeeded > freeSpaceInString,
    // rather than >= as "man snprintf" would suggest, for the same reason.
    int charsNeeded = vsnprintf( &theString->string[theString->length], freeSpaceInString+1, format, args );
    if( charsNeeded > freeSpaceInString ) {
        // snprintf *wanted* to write more characters than there was room for. Enlarge the buffer and try again.
        // Note that charsNeeded doesn't include 1 for the terminate '\0', but we don't need to add that because
        // expandStringIfNecessary(...) adds one for it internally (which is not included in theString->capacity).
        expandStringIfNecessary( theString, theString->length + charsNeeded, ScaleUpByTwo );
        freeSpaceInString = theString->capacity - theString->length;
        va_end( args );
        va_start( args, format );
        if( vsnprintf( &theString->string[theString->length], freeSpaceInString+1, format, args ) != charsNeeded ) {
            fprintf( stderr, "Warning: formattedAppend(...) returned a different answer on the second try (line %d in file %s)...\n", __LINE__, __FILE__ );
        }
    }
    va_end( args );

    theString->length += charsNeeded;

    return;
}

// 0 <= idx <= lengthOfString(desString)
void _insertCharsIntoStringAtIdx( char * fileName, int lineNumber, char * srcChars, String * desString, int idx ) {

    crashIfArgIsNullWithMessage( fileName, lineNumber, srcChars,  "insertCharsIntoStringAtIdx(...) called with a NULL 1st argument (srcChars)."  );
    crashIfArgIsNullWithMessage( fileName, lineNumber, desString, "insertCharsIntoStringAtIdx(...) called with a NULL 2nd argument (desString)." );
    crashIfMagicCookieIsBad(     fileName, lineNumber, desString, "insertCharsIntoStringAtIdx(...) called with a faulty String pointer."         );

    // If idx is < length then we append blanks until idx is equal to length,
    // at which point we append chars to the destination string.
    if( idx >= desString->length ) {
        while( idx > desString->length )
            appendCharToString( ' ', desString );
        appendCharsToString( srcChars, desString );
        return;

    } else {

        // A destination index of < 0 is treated as a prepend.
        idx = ( idx < 0 ? 0 : idx );                    // Force idx to be non-negative.

        int strLen = strlen( srcChars );                // srcChars is assumed to be nul-terminated.
        expandStringIfNecessary( desString, desString->length + strLen, ScaleUpByTwo );

        int desIdx = desString->length + strLen - 1;    // Where the rightmost char in desString->string should end up.
        int srcIdx = desString->length          - 1;    // Where the rightmost char in desString->string is initially.
        while( srcIdx >= idx ) {                        // Down to and including the position at which srcChars is to go.
            // Shift the contents of desString->string[idx...] right strLen positions.
            desString->string[desIdx--] = desString->string[srcIdx--];
        }

        srcIdx = 0;                                     // Pos of the 1st char in srcChars
        desIdx = idx;                                   // Where that 1st char goes in desString->string.
        while( srcIdx < strLen ) {
            desString->string[desIdx++] = srcChars[srcIdx++];
        }

        desString->length += strLen;
        desString->string[ desString->length ] = '\0';
    }
}

// 0 <= idx <= lengthOfString(desString)
void _insertStringIntoStringAtIdx( char * fileName, int lineNumber, String * srcString, String * desString, int idx ) {

    crashIfArgIsNullWithMessage( fileName, lineNumber, srcString, "insertStringIntoStringAtIdx(...) called with a NULL 1st argument (srcString)." );
    crashIfArgIsNullWithMessage( fileName, lineNumber, desString, "insertStringIntoStringAtIdx(...) called with a NULL 2nd argument (desString)." );
    crashIfMagicCookieIsBad(     fileName, lineNumber, srcString, "insertStringIntoStringAtIdx(...) called with a faulty src String pointer."     );
    crashIfMagicCookieIsBad(     fileName, lineNumber, desString, "insertStringIntoStringAtIdx(...) called with a faulty des String pointer."     );

    insertCharsIntoStringAtIdx( srcString->string, desString, idx );
}

void _setCapacityOfStringToInt( char * fileName, int lineNumber, String * theString, int newLength ) {

    crashIfArgIsNullWithMessage( fileName, lineNumber, theString, "setLengthOfStringToInt(...) called with a NULL String argument."    );
    crashIfMagicCookieIsBad(     fileName, lineNumber, theString, "setCapacityOfStringToInt(...) called with a faulty String pointer." );

    newLength = ( newLength < 0 ? 0 : newLength );

    if(         newLength < theString->capacity ) {
        // The +1 is space for '\0', which isn't included in length or capacity.
        theString->string = (char *) realloc( theString->string, newLength+1 );
        if( theString->string == NULL ) {
            fprintf( stderr, "ERROR in setLengthOfStringToInt(...) attempting to reduce memory for the string field." );
            exit(2);
        }
        // Adjust the length field and nul-terminate the string if we've truncated it.
        if( theString->length > newLength ) {
            theString->length = newLength;
            theString->string[newLength] = '\0';
        }
        theString->capacity = newLength;

    } else if ( newLength > theString->capacity ) {
        expandStringIfNecessary( theString, newLength, ScaleUpByOne );
    }
}

void _shrinkLengthOfStringToInt( char * fileName, int lineNumber, String * aStr, int newLength ) {

    crashIfArgIsNullWithMessage( fileName, lineNumber, aStr, "shrinkLengthOfStringToInt(...) called with a NULL String argument."  );
    crashIfMagicCookieIsBad(     fileName, lineNumber, aStr, "shrinkLengthOfStringToInt(...) called with a faulty String pointer." );

    newLength = ( newLength < 0 ? 0 : newLength );

    if( newLength < aStr->length ) {
        aStr->length = newLength;
        aStr->string[newLength] = '\0';
    }
}

int _compareStrings( char * fileName, int lineNumber, String * strA, String * strB ) {

    crashIfArgIsNullWithMessage( fileName, lineNumber, strA, "compareTheTwoStrings(...) called with a NULL 1st String argument."  );
    crashIfMagicCookieIsBad(     fileName, lineNumber, strA, "compareTheTwoStrings(...) called with a faulty 1st String pointer." );
    crashIfArgIsNullWithMessage( fileName, lineNumber, strB, "compareTheTwoStrings(...) called with a NULL 2nd String argument."  );
    crashIfMagicCookieIsBad(     fileName, lineNumber, strB, "compareTheTwoStrings(...) called with a faulty 2nd String pointer." );

    int maxLength = ( strA->length > strB->length ? strA->length : strB->length );

    return strncmp( strA->string, strB->string, maxLength );
}

int _compareStringWithChars( char * fileName, int lineNumber, String * str, char * chars ) {

    crashIfArgIsNullWithMessage( fileName, lineNumber, str,   "compareStringWithChars(...) called with a NULL String argument."  );
    crashIfMagicCookieIsBad(     fileName, lineNumber, str,   "compareStringWithChars(...) called with a faulty String pointer." );
    crashIfArgIsNullWithMessage( fileName, lineNumber, chars, "compareStringWithChars(...) called with a NULL chars argument."  );

    return strncmp( str->string, chars, str->length + 1 );
}

bool _areStringsEqual( char * fileName, int lineNumber, String * strA, String * strB ) {

    crashIfArgIsNullWithMessage( fileName, lineNumber, strA, "compareTheTwoStrings(...) called with a NULL 1st String argument."  );
    crashIfMagicCookieIsBad(     fileName, lineNumber, strA, "compareTheTwoStrings(...) called with a faulty 1st String pointer." );
    crashIfArgIsNullWithMessage( fileName, lineNumber, strB, "compareTheTwoStrings(...) called with a NULL 2nd String argument."  );
    crashIfMagicCookieIsBad(     fileName, lineNumber, strB, "compareTheTwoStrings(...) called with a faulty 2nd String pointer." );

    return (strA->length == strB->length) && (strncmp(strA->string,strB->string,strA->length) == 0);
}

bool _areStringAndCharsEqual( char * fileName, int lineNumber, String * str, char * chars ) {

    crashIfArgIsNullWithMessage( fileName, lineNumber, str,   "areStringAndCharsEqual(...) called with a NULL String argument."  );
    crashIfMagicCookieIsBad(     fileName, lineNumber, str,   "areStringAndCharsEqual(...) called with a faulty String pointer." );
    crashIfArgIsNullWithMessage( fileName, lineNumber, chars, "areStringAndCharsEqual(...) called with a NULL chars argument."   );

    return strncmp( str->string, chars, str->length+1 ) == 0;
}

void _freeString( char * fileName, int lineNumber, String * theString ) {
    crashIfArgIsNullWithMessage( fileName, lineNumber, theString, "freeString(...) called with a NULL argument."         );
    crashIfMagicCookieIsBad(     fileName, lineNumber, theString, "freeString(...) called with a faulty String pointer." );
    free( theString->string );
    free( theString         );
}

// ------------------------------------------------------------------------------------------------
//
//     Public callback routines.
//
//     Most likely to be useful in conjunction with
//
//         listToStringUsingFunction(...)
//         destroyList(...)
//         destroyDictionary(...)
//
//     EG if the items in a dictionary d are GStrings, then you would call
//
//         destroyDictionary( d, freeGstring_callback );
//
//     For each item foo in d, destroyDictionary(...) would then call freeGString_callback(foo)
//     to free the GString. Needed because you can put a pointer to any sort of thing you want
//     into a dictionary, which therefore can have no idea how to free whatever it is you've put
//     into it. So you have provide a routine it can call that knows what to do. Of course if
//     you've put something other than a CString or "UnixString" ( ie a nul-terminated array of
//     chars), you'll have to write from a scratch a routine with the same signature that does
//     the right thing.
//
//     Similarly for Lists.
//
//     Analogously for listToStringUsingFunction(...) - only you know what you've got a list of,
//     so only you can supply a function to create a String representation of it. If it happens
//     that you have a list of GStrings, you can just use makeGStringFromChars(...); if you've
//     got a list of something else, you'll have to write from a scratch a routine with the same
//     signature that does the same thing.
//
//     Warning - you can't use freeString(...) instead of freeCString_callback because there's a
//     bunch of C preprocessor macro stuff in GString.h that converts a call to freeString(arg)
//     into a call of _freeString(fileName,lineNum,arg), which is what's actually implemented
//     in GString.c.
//
// ------------------------------------------------------------------------------------------------

// Provides a 1-arg interface to newStringFromChars(...),
// which cpp turns into a 3-arg interface.
StringPointer makeGStringFromCString_callback( void * str ) {
    return newStringFromChars( str );
}

// StringPointer makeCopyOfGString( void * str ) {
//     return newStringFromString( (String*) str );
// }

void freeCString_callback( void * str ) {
    free( str );
}
void freeGString_callback( void * ptr ) {
    String * gstr = (String*) ptr;
    free( gstr->string );
    free( gstr         );
}

// --------------------------------------------------------------------------------------
//
//     Private helpers.
//
// --------------------------------------------------------------------------------------

static    // So that this routine is invisible to code not physically in this file.
void crashIfArgIsNullWithMessage( char * fileName, int lineNumber, void * ptr, char * msg ) {
    if( ptr == NULL ) {
        fprintf( stderr, "ERROR on line %d of file %s: %s\n", lineNumber, fileName, msg );
        exit(1);
    }
}

static   // So that this routine is invisible to code not physically in this file.
void expandStringIfNecessary( String * theString, int sizeNeeded, int expansionFactor ) {

    // For safety's sake... Space for a '\0' is added below.
    sizeNeeded = ( sizeNeeded < 0 ? 0 : sizeNeeded );

    if( sizeNeeded > theString->capacity ) {

        // sizeNeeded is assumed NOT to include space for the terminating '\0', for which we add 1 below.
        if( theString->capacity == 0 )
            theString->capacity  = sizeNeeded;
        else {
            theString->capacity  = expansionFactor * sizeNeeded;
        }

        // We need space for theString->length, theString->capacity, theString->capacity chars, and +1 for a '\0'.
        int sizeOfBlock = (theString->capacity * sizeof(char)) + 1;
        if( DEBUG ) fprintf( stderr, "expandStringIfNecessary(...) is requesting %d bytes for its string field.\n", sizeOfBlock );
        theString->string = (char*) realloc( theString->string, sizeOfBlock );

        if( theString->string == NULL ) {
            fprintf( stderr, "ERROR in expandStringIfNecessary(...) attempting to reallocate memory for the string field." );
            exit(2);
        }
    }
    return;
}

static   // Used to verify that the magic cookie with which initialized StringStructs are tagged is correct.
void crashIfMagicCookieIsBad( char * fileName, int lineNumber, StringPointer ptr, char * msg ) {
    if( ptr->magicCookie.mcIntVal != ssMagicCookieValue.mcIntVal ) {
        fprintf( stderr, "ERROR on line %d of file %s: %s\n", lineNumber, fileName, msg );
        exit(2);
    }
}
