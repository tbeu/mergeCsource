/*
 *  ListArray.c
 *
 *  Created by jcbeatty.
 *  Copyright 2009 University of Waterloo. All rights reserved.
 *
 */

// --------------------------------------------------------------------------------------
//
//     Growable partially-filled native arrays of pointers.
//
// --------------------------------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "List.h"

// For monitoring the heap. Must *follow* stdio and stdlib.
#ifdef DMALLOC
/*mergeCsource:forceIncludeCounter--*/     // Used by mergeCsource - don't touch!
#include "dmallocFor241.h"
/*mergeCsource:forceIncludeCounter++*/     // Used by mergeCsource - don't touch!
#endif

#define DEBUG_LIST_ARRAY false

// --------------------------------------------------------------------------------------
//
//    Data structures.
//
// --------------------------------------------------------------------------------------

// The "cookie" field is set to a special bit pattern (ie the 64 bits of ASCII for "ListAray") by the ListArray constructors;
// the ListArray methods then abort with an error message if they're passed an alleged ListStruct whose cookie field doesn't match.
// It's a union so that we can easily set the cookie to the 8 chars we want (using strVal), and efficiently test a ListStruct's
// cookie with a single integer comparison (using intVal). Note that according to ANSI C, a long long is a least 8 bytes in size.
typedef union { char strVal[8]; long long intVal; } LA_Cookie;                                 // ListArray_Cookie

static LA_Cookie la_CookieValue = { .strVal = { 'L', 'i', 's', 't', 'A', 'r', 'a', 'y' } };    // "ListAray"

// Each of these is a growable array of pointers.
typedef struct ListStruct_ {
    LA_Cookie  cookie;           // For verifying that List Array routines have actually been passed an initialized ListStruct.
    char *     calledFromFile;   // Name of the file from which the constructor that allocated this ListStruct was called. (Located in the stack.)
    int        calledFromLine;   // Line number in calledFromFile from which the constructor that allocatd this ListStruct was called.
    int        length;           // The number of elements currently in the array (ie in data[0..length-1]).
    int        capacity;         // The number of slots currently in the array; only elts [0..length-1] are in use.
    void *     (*ptrs)[];        // Ptrs is a pointer to an array of pointers to whatever's in the list.
} ListArrayOfPointers;

// "typedef struct ListStruct_ ListStruct, * ListPointer, * ListPtr" appears in List.h.

// -------------------------------------------------

typedef enum { LAI_FORWARD, LAI_REVERSE } LAI_Direction;

static LA_Cookie la_Iter_cookieValue = { .strVal = { 'L', 'A', '_', 'I', 't', 'e', 'r', 'C' } };  // "LA_IterC"

struct ListIteratorStruct_ {
    LA_Cookie            cookie;         // For verifying that ListIterator routines have actually been passed an initialized ListIteratorStruct.
    LAI_Direction        direction;      // Are we enumerating forward from the head of the list or backwards from its tail.
    struct ListStruct_ * list;           // The list through which we are iterating.
    int                  currentIndex;   // The index of the next element to be enumerated (ie returned by nextListElement(...)).
};

typedef struct ListIteratorStruct_ ListIterator;

// --------------------------------------------------------------------------------------
//
//     Global variables (global within this file, that is).
//
// --------------------------------------------------------------------------------------

// Nothing at present...

// --------------------------------------------------------------------------------------
//
//     Private helpers (definitions are at the end of the file).
//
// --------------------------------------------------------------------------------------

static void la_crashIfPtrIsNULL(            char * fileName, int lineNumber,          void *          ptr,      char * msg                 );
static void la_crashIfListArrayCookieIsBad( char * fileName, int lineNumber,          ListPointer     theArray, char * msg                 );
static void la_crashIfIndexNotInRange(      char * fileName, int lineNumber, int idx, ListPointer     theArray, char * msg                 );
static void la_crashIfListIterCookieIsBad(  char * fileName, int lineNumber,          ListIteratorPtr ptr,                 char * function );
static void la_ensureCapacityIsAtLeast( ListPointer theArray, int sizeNeeded );

// --------------------------------------------------------------------------------------
//
//     Constructors.
//
// --------------------------------------------------------------------------------------

ListPointer _newEmptyList( char * fileName, int lineNumber, ListImplementation implementation ) {
    
    // It's an error to request an implementation that's not available.
    if( implementation != UseDefaultList && implementation != UseListArray ) {
        fprintf( stderr, "ERROR in call to newEmptyList(...) on line %d of file %s.\n", lineNumber, fileName      );
        fprintf( stderr, "You have requested a List implementation that is not available.  Aborting execution.\n" );
        fprintf( stderr, "(Try using UseDefaultList or UseListArray instead.)\n"                                  );
        exit(1);
    } 
    
    ListPointer newArray = (ListPointer) malloc( sizeof(ListArrayOfPointers) );
    newArray->cookie.intVal  = la_CookieValue.intVal;
    newArray->calledFromFile = fileName;
    newArray->calledFromLine = lineNumber;
    newArray->length         = 0;
    newArray->capacity       = 0;
    newArray->ptrs           = NULL;
    return newArray;
}

ListPointer _newListWithCapacity( char * fileName, int lineNumber, ListImplementation implementation, int sizeWanted ) {
    ListPointer theArray = _newEmptyList( fileName, lineNumber, implementation );
    la_ensureCapacityIsAtLeast( theArray, sizeWanted );
    return theArray;
}

ListPointer _newListFromFixedArrayOfPointers( char * fileName, int lineNumber, ListImplementation implementation, void * cArray[], int length )
{
    int idx;
        
    la_crashIfPtrIsNULL( fileName, lineNumber, cArray, "newListFromFixedArrayOfPointers' C array pointer" );
    ListPointer newArray = _newListWithCapacity( fileName, lineNumber, implementation, length );
    for( idx = 0; idx < length; idx++ ){
        _addPtrToEndOfList( fileName, lineNumber, cArray[idx], newArray );
    }
    return newArray;
}

// --------------------------------------------------------------------------------------
//
//     Public methods.
//
// --------------------------------------------------------------------------------------

#define versionLAstrLen 100
static char la_version[versionLAstrLen];

char * _listVersion( char * fileName, int lineNumber, ListPointer theList ) {
    // If you change versionLAstrLen, take care that it's long enough to hold what's written into it by snprintf(...),
    // an example of which is the 70 character string "List (Array) version 1.1 (compiled on Jun 24 2009 at 05:34:24 hours)\0".
    // The C preprocessor will replace __DATE__ and __TIME__ with appropriate literal strings.
    #ifdef DMALLOC
        char * dmallocFlag = "w/dmalloc ";
    #else
        char * dmallocFlag = "";
    #endif
    snprintf( la_version, versionLAstrLen, "List (Array) version 2.0 %s(compiled on %s at %s hours)", dmallocFlag, __DATE__, __TIME__ );
    return la_version;
}

#undef versionLAstrLen

void _insertPtrAtBeginningOfList( char * fileName, int lineNumber, void * ptr, ListPointer theArray ) {
    la_crashIfPtrIsNULL(            fileName, lineNumber, ptr,      "insertPtrAtBeginningOfList's element pointer"                                    );
    la_crashIfPtrIsNULL(            fileName, lineNumber, theArray, "insertPtrAtBeginningOfList's list pointer"                                       );
    la_crashIfListArrayCookieIsBad( fileName, lineNumber, theArray, "insertPtrAtBeginningOfList(...) called with a List pointer whose cookie is bad." );
    return _insertPtrIntoListAtIndex( fileName, lineNumber, ptr, theArray, 0 );
}

// Pre:  0 <= index <= size(). If index == length, we're actually appending a ptr to the array, which is allowed.
// Post: Items from index to length-1 have their index increased by one (ie they move right one position), and
//       ptr is inserted at position index.
void _insertPtrIntoListAtIndex( char * fileName, int lineNumber, void * ptr, ListPointer theArray, int index )
{
    int idx;
    
    la_crashIfPtrIsNULL(            fileName, lineNumber, ptr,      "insertPtrIntoListAtIndex's element pointer"                                    );
    la_crashIfPtrIsNULL(            fileName, lineNumber, theArray, "insertPtrIntoListAtIndex's list parameter"                                     );
    la_crashIfListArrayCookieIsBad( fileName, lineNumber, theArray, "insertPtrIntoListAtIndex(...) called with a List pointer whose cookie is bad." );
    
    if( index == theArray->length ) {
        return _addPtrToEndOfList( fileName, lineNumber, ptr, theArray );
    }
    
    // Verify that index is in [0,length) - since we've already handled the case index == length, that's just what we want.
    la_crashIfIndexNotInRange(     fileName, lineNumber, index, theArray, "function insertPtrIntoListAtIndex"                                         );
    
    la_ensureCapacityIsAtLeast( theArray, theArray->length + 1 );
    
    // Shift the pointers at index through length-1 right one position, leaving position index empty.
    for(   idx = theArray->length;   idx > index;   idx--   ) {
        (*theArray->ptrs)[idx] = (*theArray->ptrs)[idx-1];
    }
    
    // Place ptr in the now-free position.
    (*theArray->ptrs)[index] = ptr;
    theArray->length++; 
    
    return;
}

void _addPtrToEndOfList( char * fileName, int lineNumber, void * ptr, ListPointer theArray )
{
    la_crashIfPtrIsNULL(            fileName, lineNumber, ptr,      "addPtrToEndOfList's element parameter"                                  );
    la_crashIfPtrIsNULL(            fileName, lineNumber, theArray, "addPtrToEndOfList's list parameter"                                     );
    la_crashIfListArrayCookieIsBad( fileName, lineNumber, theArray, "addPtrToEndOfList(...) called with a List pointer whose cookie is bad." );
    la_ensureCapacityIsAtLeast( theArray, theArray->length + 1 );
    
    (*theArray->ptrs)[theArray->length] = ptr; 
    theArray->length++;
    
    return;
}

// Pre:  theArray != NULL and 0 <= idx < theArray->length.
// Post: returns the poinnter at position idx in theArray->ptrs.
void * _getPtrAtIndexInList( char * fileName, int lineNumber, int idx, ListPointer theArray )
{
    la_crashIfPtrIsNULL(            fileName, lineNumber,      theArray, "getPtrAtIndexInList's list parameter"                                     );
    la_crashIfIndexNotInRange(      fileName, lineNumber, idx, theArray, "function getPtrAtIndexInList"                                             );
    la_crashIfListArrayCookieIsBad( fileName, lineNumber,      theArray, "getPtrAtIndexInList(...) called with a List pointer whose cookie is bad." );
    return (*theArray->ptrs)[idx];  
}

// Pre:  0 <= index < lengthOfArray().
// Post: Items from index to length-1 have their index decreased by one (ie they move left one position), and
//       the ptr that used to be at position idx is returned.
void * _removeAndReturnPtrAtIndexFromList( char * fileName, int lineNumber, int index, ListPointer theArray )
{
    int idx;
    
    la_crashIfPtrIsNULL(            fileName, lineNumber,        theArray, "removeAndReturnPtrAtIndexFromList's list parameter"                                     );
    la_crashIfListArrayCookieIsBad( fileName, lineNumber,        theArray, "removeAndReturnPtrAtIndexFromList(...) called with a List pointer whose cookie is bad." );
    la_crashIfIndexNotInRange(      fileName, lineNumber, index, theArray, "function removeAndReturnPtrAtIndexFromList"                                             );
    
    void * answer = (*theArray->ptrs)[index];
    
    theArray->length--; 
    
    // Shift the pointers at index through (the initial) length-1 left one position, leaving the array one entry smaller.
    for(   idx = index;   idx < theArray->length;   idx++   ) {
        (*theArray->ptrs)[idx] = (*theArray->ptrs)[idx+1];
    }
    
    return answer;
}

// Pre:  0 <= index <= size(). If index == length, we're actually appending a ptr to the array, which is allowed.
// Post: Items from index to length-1 have their index increased by one (ie they move right one position), and
//       ptr is inserted at position index.
void * _replaceAndReturnPtrAtIndexInList( char * fileName, int lineNumber, void * ptr, int index, ListPointer theArray )
{   
    la_crashIfPtrIsNULL(            fileName, lineNumber, ptr,              "replaceAndReturnPtrAtIndexInList's element parameter"                                  );
    la_crashIfPtrIsNULL(            fileName, lineNumber,         theArray, "replaceAndReturnPtrAtIndexInList's list parameter"                                     );
    la_crashIfListArrayCookieIsBad( fileName, lineNumber,         theArray, "replaceAndReturnPtrAtIndexInList(...) called with a List pointer whose cookie is bad." );
    la_crashIfIndexNotInRange(      fileName, lineNumber, index,  theArray, "function replaceAndReturnPtrAtIndexInList"                                             );
    void * answer = (*theArray->ptrs)[index];
    (*theArray->ptrs)[index] = ptr;
    return answer;
}

int _capacityOfList( char * fileName, int lineNumber, ListPointer theArray ) {
    la_crashIfPtrIsNULL(            fileName, lineNumber, theArray, "capacityOfList"                                                      );
    la_crashIfListArrayCookieIsBad( fileName, lineNumber, theArray, "capacityOfList(...) called with a List pointer whose cookie is bad." );
    return theArray->capacity;
}

int _lengthOfList( char * fileName, int lineNumber, ListPointer theArray ) {
    la_crashIfPtrIsNULL(            fileName, lineNumber, theArray, "lengthOfList"                                                      );
    la_crashIfListArrayCookieIsBad( fileName, lineNumber, theArray, "lengthOfList(...) called with a List pointer whose cookie is bad." );
    return theArray->length;
}

bool _isListEmpty( char * fileName, int lineNumber, ListPointer theArray ) {
    la_crashIfPtrIsNULL(            fileName, lineNumber, theArray, "isListEmpty"                                                                     );
    la_crashIfListArrayCookieIsBad( fileName, lineNumber, theArray, "insertPtrAtBeginningOfList(...) called with a List pointer whose cookie is bad." );
    return theArray->length == 0;
}

// All extent entries are free'd (if fcn is supplied) and the length is set to 0, but the ptrs array is NOT released, so the CAPACITY is unchanged.
void _makeListEmpty( char * fileName, int lineNumber, ListPointer theArray, FreeDataFcn * fcn ) {
    
    int idx;

    la_crashIfPtrIsNULL(            fileName, lineNumber, theArray, "_makeListEmpty"                                                                  );
    la_crashIfListArrayCookieIsBad( fileName, lineNumber, theArray, "insertPtrAtBeginningOfList(...) called with a List pointer whose cookie is bad." );

    if( fcn != NULL ) {
        for( idx = 0; idx < theArray->length; idx++ ) {
            fcn( (*theArray->ptrs)[idx] );    // Call user-supplied function to free heap memory for user's data (including the ptr itself).
        }
    }
    
    theArray->length = 0;
    
    return;
}

void _destroyList( char * fileName, int lineNumber, ListPointer theArray, FreeDataFcn * fcn ) {
    la_crashIfPtrIsNULL(            fileName, lineNumber, theArray, "destroyList"                                                      );
    la_crashIfListArrayCookieIsBad( fileName, lineNumber, theArray, "destroyList(...) called with a List pointer whose cookie is bad." );
    _makeListEmpty( fileName, lineNumber, theArray, fcn );
    if( theArray->ptrs != NULL )     // We expect it will NOT be NULL.
        free( theArray->ptrs );
    free( theArray );
}

// Pre:  theArray != null, and is a homogeneous array of pointers to some particular struct.
//       aToStringFunction(...) takes as argument a pointer to a struct of whatever theArray contains pointers to and returns a String representation of that struct.
//       Prefix, gap and suffix may be null( = ""). Returns a string surrounded by prefix and suffix, with gap separating the strings generated for each element.
String * _listToStringUsingFunction( char * fileName, int lineNumber, ListPointer theArray, ToStringFcn * aToStringFunction, char * prefix, char * gap, char * suffix )
{
    int idx;
    la_crashIfPtrIsNULL(            fileName, lineNumber, theArray, "destroyList's list parameter is NULL"                                           );
    la_crashIfListArrayCookieIsBad( fileName, lineNumber, theArray, "listToStringUsingFunction(...) called with a List pointer whose cookie is bad." );
    char   * curGap = "";
    String * answer = newEmptyStringWithCapacity( 100 );
    if( prefix != NULL )
        appendCharsToString( prefix, answer );
    for( idx = 0; idx < theArray->length; idx++ ) {
        if(  gap != NULL )
            appendCharsToString( curGap, answer );
        if( aToStringFunction == NULL ) {
            // Entries in the array ARE Strings.
            appendStringToString( (*theArray->ptrs)[idx], answer );
        } else {
            // The user-supplied function is assumed to create new String describing the contents of the array element. 
            String * piece;
            piece = aToStringFunction( (*theArray->ptrs)[idx] );
            appendStringToString( piece, answer );
            freeString( piece );
        }
        curGap = gap;
    }
    if( suffix != NULL )
        appendCharsToString( suffix, answer );
    return answer;
}

// --------------------------------------------------------------------------------------
//
//    Iterators.
//
// --------------------------------------------------------------------------------------

ListIteratorPtr _newForwardListIterator( char * fileName, int lineNumber, ListPointer theArray ) {
    la_crashIfPtrIsNULL(            fileName, lineNumber, theArray, "newForwardListIterator's list parameter is NULL"                             );
    la_crashIfListArrayCookieIsBad( fileName, lineNumber, theArray, "newForwardListIterator(...) called with a List pointer whose cookie is bad." );
    ListIteratorPtr newIterator = (ListIteratorPtr) malloc( sizeof(ListIterator) );
    newIterator->cookie.intVal  = la_Iter_cookieValue.intVal;
    newIterator->direction      = LAI_FORWARD;
    newIterator->list           = theArray;
    newIterator->currentIndex   = 0;
    return newIterator;
}

ListIteratorPtr _newBackwardListIterator( char * fileName, int lineNumber, ListPointer theArray ) {
    la_crashIfPtrIsNULL(            fileName, lineNumber, theArray, "newBackwardListIterator's list parameter is NULL"                             );
    la_crashIfListArrayCookieIsBad( fileName, lineNumber, theArray, "newBackwardListIterator(...) called with a List pointer whose cookie is bad." );
    ListIteratorPtr newIterator = (ListIteratorPtr) malloc( sizeof(ListIterator) );
    newIterator->cookie.intVal  = la_Iter_cookieValue.intVal;
    newIterator->direction      = LAI_REVERSE;
    newIterator->list           = theArray;
    newIterator->currentIndex   = theArray->length - 1;
    return newIterator;
}

bool _listHasNextElement( char * fileName, int lineNumber, ListIteratorPtr theIterator ) {
    la_crashIfPtrIsNULL(           fileName, lineNumber, theIterator, "listHasNextElement's iterator parameter is NULL" );
    la_crashIfListIterCookieIsBad( fileName, lineNumber, theIterator,  __FUNC__                                         );
    int index = theIterator->currentIndex;
    return index >= 0 && index < theIterator->list->length;
}

void * _nextListElement( char * fileName, int lineNumber, ListIteratorPtr theIterator ) {

    la_crashIfPtrIsNULL(           fileName, lineNumber, theIterator, "nextListElement's iterator parameter is NULL" );
    la_crashIfListIterCookieIsBad( fileName, lineNumber, theIterator,  __FUNC__                                      );
    
    if( _listHasNextElement(fileName, lineNumber, theIterator) ) {
        void * answer = (*theIterator->list->ptrs)[ theIterator->currentIndex ];
        if(        theIterator->direction == LAI_FORWARD ) {
            theIterator->currentIndex++;
            return answer;
        } else if( theIterator->direction == LAI_REVERSE ) {
            theIterator->currentIndex--;
            return answer;
        } else {
            fprintf( stderr, "ERROR in nextListElement(...), called from line %d of file %s.\n", lineNumber, fileName     );
            fprintf( stderr, "The direction for this list is neither FORWARD or BACKWARD, which shouldn't ever happen!\n" );
            exit(1);
        }
        
    } else {
        // Off the end.
        return NULL;
    }
}

void _destroyIterator( char * fileName, int lineNumber, ListIteratorPtr theIterator ) {
    la_crashIfPtrIsNULL(           fileName, lineNumber, theIterator, "destroyIterator's iterator parameter is NULL" );
    la_crashIfListIterCookieIsBad( fileName, lineNumber, theIterator,  __FUNC__                                      );
    free( theIterator );
    return;
}

// For debugging purposes - in particular, for chasing down an unfree'd block of heap memory that you know to be a ListStruct,
// but for which you want to know *which* ListStruct. The expectation is that you've got the address of the block, assign that
// to a dummy ListPointer with a cast, then call this routine (or use an interactive debugger) to answe that questions.
// Of course, if you hand it the wrong address you may crash the program...
void la_printMsgAndListOrigin( char * msg, ListPointer theList ) {
    fprintf( stderr, "%s: called from line %d in file %s.\n", msg, theList->calledFromLine, theList->calledFromFile );
}

// --------------------------------------------------------------------------------------
//
//     Private helpers.
//
// --------------------------------------------------------------------------------------

static    // Accessible only from other routines in this file.
void la_crashIfIndexNotInRange( char * fileName, int lineNumber, int idx, ListPointer theArray, char * msg ) {
    if( (0 <= idx) && (idx < theArray->length) ) {
        return;
    } else {
        fprintf( stderr, "ERROR: out-of-range index %d on line %d in file %s (%s). Aborting.\n", idx, lineNumber, fileName, msg );
        exit(1);
    }
}

static    // Accessible only from other routines in this file.
void la_crashIfPtrIsNULL( char * fileName, int lineNumber, void * ptr, char * msg ) {
    if( ptr != NULL ) {
        return;
    } else {
        fprintf( stderr, "ERROR: attempt to insert a NULL pointer into the list on line %d of file %s (%s).\n", lineNumber, fileName, msg );
        exit(1);
    }
}

static   // Accessible only from other routines in this file.
void la_ensureCapacityIsAtLeast( ListPointer theArray, int sizeNeeded ) {
    
    // According to wikipedia, "realloc behaves like malloc if the first argument is NULL"
    // (http://en.wikipedia.org/wiki/Realloc#realloc). And this agrees with the gnu C documentation.
    // However, earlier C libraries may not behave this way, so we treat NULL (capacity == 0) separately.
    
    // Silently ensure that sizeNeede >= 0.
    sizeNeeded = ( sizeNeeded < 0 ? 0 : sizeNeeded );
    
    if( sizeNeeded > theArray->capacity ) {
        if( theArray->capacity == 0 ) {
            
            // The first allocation - provide exactly what's requested.
            theArray->capacity = sizeNeeded;
            
            int sizeOfNewBlock = theArray->capacity * sizeof(void*);
            if( DEBUG_LIST_ARRAY ) fprintf( stderr, "la_ensureCapacityIsAtLeast(...) is requesting %d bytes.\n", sizeOfNewBlock );
            theArray->ptrs = malloc( sizeOfNewBlock );
            
            if( theArray->ptrs == NULL ) {
                fprintf( stderr, "ERROR in expandListArrayIfNecessary(...) attempting to allocate memory to %d bytes. Aborting.", sizeOfNewBlock );
                exit(2);
            }
            
        } else {
            
            // We're enlarging an existing array - provide extra space for future growth.
            theArray->capacity  = 2 * sizeNeeded;
            
            int sizeOfNewBlock = theArray->capacity * sizeof(void*);
            if( DEBUG_LIST_ARRAY ) fprintf( stderr, "la_ensureCapacityIsAtLeast(...) is requesting %d bytes.\n", sizeOfNewBlock );
            theArray->ptrs = realloc( theArray->ptrs, sizeOfNewBlock );
            
            if( theArray->ptrs == NULL ) {
                fprintf( stderr, "ERROR in expandListArrayIfNecessary(...) attempting to reallocate memory to %d bytes. Aborting.", sizeOfNewBlock );
                exit(2);
            }
        }
    }
    
    return;
}

static   // Used to verify that the cookie with which initialized ListStructs are tagged is correct.
void la_crashIfListArrayCookieIsBad( char * fileName, int lineNumber, ListPointer ptr, char * msg ) {
    if( ptr->cookie.intVal != la_CookieValue.intVal ) {
        fprintf( stderr, "ERROR on line %d of file %s: %s\n", lineNumber, fileName, msg );
        exit(2);
    }
}

#ifdef DMALLOC
// Externally visible but not documented it's for debugging purposes only, and is specific to this List implementation.
// Returns the space dynamically allocated for the partially-filled array containing ptrs.
// (And not called from elsewhere within this file.)
int la_bytesAllocatedForListPtrsPerDMalloc(/* char * fileName, int lineNumber, */ ListPointer heapPtr ) {
    
    DMALLOC_SIZE userSize;                   // Bytes allocated to hold data.
    DMALLOC_SIZE totalSize;                  // Total bytes allocated, including administrative overhead.
    
    if( heapPtr->ptrs == NULL ) return 0;    // Will be if the List is new or has been emptied.
    
    // The other parameters, if not null, return other info we're not interested in.
    if( dmalloc_examine( heapPtr->ptrs, &userSize, &totalSize, NULL, NULL, NULL, NULL, NULL ) == DMALLOC_ERROR ) {
        fprintf( stderr, "dmalloc_examine error." );
        return -1;
    } else {
        return userSize;
    }
}
#endif

static   // Used to verify that the cookie with which initialized ListIteratorStructs are tagged is correct.
void la_crashIfListIterCookieIsBad( char * fileName, int lineNumber, ListIteratorPtr ptr, char * function ) {
    if( ptr->cookie.intVal != la_Iter_cookieValue.intVal ) {
        fprintf( stderr, "ERROR: the ListLinked ListIterator pointer passed to %s on line %d of the file %s has a bad cookie. Aborting execution.\n", function, lineNumber, fileName );
        exit(2);
    }
}

#undef DEBUG_LIST_ARRAY
