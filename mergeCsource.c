/*
 *  mergeCsource.c
 *
 *  This program is a very simple inclusion processor, intended for use in CS 241. The files listed on the commmand line
 *  are concatenated together in the order listed; the content of files named by #include "..."'s are also incorporated,
 *  replacing the triggering #include. Files named by a #include <...> are NOT incorporated - these are system-sensitive
 *  files, and should be expanded on the system upon which the program is compiled, which may not be identical to the
 *  system on which the source files were merged by this program, and from which the file was submitted to marmoset.
 *
 *  Any number of "user include directories" may be named on the command line, each preceded by -I (to be compatible with cpp).
 *
 *  The location of files and include directories may be specified by either relative or absolute paths; the former must be
 *  relative to the working directory. A special case, of course, is simply the name of a file in the working directory.
 *
 *  Each file appearing on the command line is preceded in the output by three lines naming the file. EG:
 *
 *      //-------------------------------
 *      //---------- ../../FileOne.text
 *      //-------------------------------
 *
 *  By default only the first appearance of a user include statement is replaced by the referenced file,
 *  effectively implementing the usual "#ifdef FOO\n#define FOO...#endif" technique but without attempting to parse
 *  #... statements and implement the relevant subset of cpp's macro processing. Usually this is all you need;
 *  however, the List and Dictionary modules do some fancy stuff with macros in the ListAll.c and DictionaryAl.c
 *  files that requires reading List.h and Dictionary.h three times. To treat them as special cases, the
 *  relevant #includes are surrounded by lines beginning with the following strings:
*/
        /*mergeCsource:forceIncludeCounter++*/

        /*mergeCsource:forceIncludeCounter--*/
/*
 *  The variable forceIncludeCounter is initialized to 0 and non-first-appearance instances of a user include *are*
 *  included iff forceIncludeCounter > 0. An execution time flag is available (-includeAll) which initializes 
 *  forceIncludeCounter to one million, effectively ensuring the *all* #include statements are replaced by the 
 *  referenced files.
 *
 *  Other execution time options available are:
 *
 *      -trackIncludesCounter       causes a msg to stderr whenever either of the mergeCsource:force... lines appears.
 *      
 *      -reportFilesSeen            causes a table listing all the user include files processed, the number of times
 *                                  each was encountered, the number of times each was actually included, and some
 *                                  other information that was useful for debugging.
 *
 *      -help                       (or --help, -h, -usage or --uwage)
 *
 *      -includeAll                 *every* instance of #include "..." is replaced by the named file, regardless
 *                                  of whether or not the file has been seen previously.
 *
 *      -version                    (or -v, --v, or --version) prints version info for mergeCsource, GString & List.
 *
 *  The option tokens are case-insensitive.
 *
 *  This program uses the CS 241 GString and List modules (see http://www.student.cs.waterloo.ca/~jcbeatty/cs241.)
 *
 *  This program determines whether it has previously seen a user include file by remembering and comparing device
 *  and inode numbers, which are obtained using fstat(...), which is part of the POSIX. Hence this program can't be
 *  be run on a system that does not provide this information about files reference by this program. 
 *
 *  Initially created by jcbeatty on 10 June 2009.
 *  Last materially modified on 29 August 2009.
 *
 */

#include <stdio.h>                  // For printf.
#include <stdlib.h>                 // To define NULL;
#include <string.h>
#include <stdbool.h>                // For bool, true, false.
#include <ctype.h>                  // For isspace(...).
#include <unistd.h>                 // For getcwd(...).
#include <sys/stat.h>               // For stat(...), fstat(...), struct stat.
#include <sys/types.h>              // For dev_t and i_not_t.
#include <sys/param.h>              // For MAXPATHLEN

#include "GString.h"
#include "List.h"

// --------------------------------------------------------------------------------------
//
//    Globals, data structures, and prototypes.
//
// --------------------------------------------------------------------------------------

// For monitoring the heap. Must *follow* stdio and stdlib.
#ifdef DMALLOC
    #include "dmallocFor241.h"
#endif

// For debugging.
#define DEBUG                  false
#define ECHO_FILES_INCLUDED    false
#define REPORT_ON_OPTS_SEEN    false

// Initialized by main and used by processFile(...).
// A global so we don't have to create one with each instance of processFile(...).
// Used as an i/o buffer to hold one line from the file currently being copied.
StringPointer lineBuffer = NULL;

// Used by processFile(...) and readNextLineFromFileIntoString(...). 
enum ReturnedValue { SomethingToDo, AllDone };

// MAXPATHLEN, defined in sys/params.h, is the maximum possible length of the absolute path to a file or directory.
// In particular, it's the maximum amount space that getwd(...) or getcwd(...) can need for its returned value.
// Only needed if we're using getcwd(...), which we're not currently because on Solaris it leaves debris in the heap.
// static char wd[MAXPATHLEN];

// This is the data stored in the filesProcessed list.
typedef struct HeaderFileInfo_ {
    dev_t     deviceNumber;      // Of the device on which the file is stored.
    ino_t     inodeNumber;       // Of the i-node on that device for the file.
    String *  path;              // Path by which we reached the file. For debugging, not for the program.
    int       seenCount;         // For debugging, not for the program.
    int       usedCount;         // Number of times actually included.
    bool      isDotH;            // True iff path ends with ".h".
} HeaderFileInfo, * HeaderFileInfoPtr;

// Prototypes - definitions are at the end of this file.
void            processFile( StringPointer embeddingSequence, StringPointer pathToFile, ListPointer incDirectories, int forceIncludeCounter, bool trackIncludesFlag, ListPointer filesProcessed );
bool            readNextLineFromFileIntoString( FILE * fp, StringPointer line );
StringPointer   doesLineStartWithLocalInclude( StringPointer line );
StringPointer   extractPathPrefixIn( StringPointer str );
ListPointer     extractAndRemoveIncludeDirectoriesFrom( ListPointer args );
bool            findAndRemoveFlagFromArgs( char * flag, ListPointer args );
StringPointer   stringFromChars( void * chars );
bool            isHeaderFileSeenBefore( const struct stat * fileInfo, StringPointer pathToFile, ListPointer filesSeen, int forceIncludeCounter );
bool            doesArgsSetFlag( ListPointer args, char * optStr );
char          * getArgForOptionIfPresent( ListPointer args, char * optStr );
void            printHelpInfo( void );

// --------------------------------------------------------------------------------------
//
//    Main.
//
// --------------------------------------------------------------------------------------

int main( int argc, char * argv[] ) {
    
    #ifdef DMALLOC
        // 1st param - one of: DMALLOC_runtimeFor241, DMALLOC_lowFor241, DMALLOC_mediumFor241, DMALLOC_highFor241.
        // 2nd param - most useful possibilities are: print-messages, log=logfile, inter=100 (separate by commas when combining).
        dmalloc_setup( DMALLOC_highFor241, "print-messages,log=logfileDmalloc" );
    #endif
    
    // Used to point to the working directory.
    char * wd = NULL;

    // We use getenv(...) rather than getcwd(...) because on Solaris (though not on OSX) getcwd(...) leaves four blocks of heap memory unfree'd.
    // However, if you port this to a third platform and getenv("PWD") doesn't work for you, try getcwd(...). The debris Solaris leaves isn't
    // actually worth worrying about, but since I had a workaround (PWD is available on both Solaris and OSX) I couldn't resist using it [jcb]...
    // Incidentally, according to Stevens & Rago ("Advanced Programming in the Unix Environment, 2/e, pp 187-88), environment variables are usually
    // stored above the stack, along with command line arguments - ie they're *not* in the heap, and therefore should/need not be free'd.
    // if( getcwd(wd,MAXPATHLEN) != NULL ) { if( DEBUG ) fprintf( stderr, "According to getcwd(...), the current working directory is %s.\n", wd ); } 
    if( (wd = getenv("PWD")) != NULL ) { 
        if( DEBUG ) fprintf( stderr, "According to getenv(\"PWD\"), the current working directory is %s.\n", wd );
        if( chdir(wd) < 0 ) {
            // The chdir is only needed when running in Xcode, which sets it to <projectRoot>/build/Debug instead of the more convenient <projectRoot>.
            // When not running in Xcode, we should be changing it to what it already is (ie deleting the call should have no semantic effect). 
            fprintf( stderr, "Failed to chdir to the working directory %s. Aborting execution.\n", wd );
            exit(1);
        }
    } 
    else {
        fprintf( stderr, "Failed to obtain the working directory. Aborting execution.\n" );
        exit(1);
    }
    
    lineBuffer = newEmptyStringWithCapacity( 100 );
    
    // args is a List of char*'s that are located in the stack, not in the heap.
    // (See "Advanced Unix Programming" by Stevens & Rago, p xxx.)
    ListPointer args = newListFromFixedArrayOfPointers( UseDefaultList, (void**) argv, argc );
    
    // Delete the path to the file we're executing.
    removeAndReturnPtrAtIndexFromList( 0, args );
    
    // Used by processFile(...) and isHeaderFileSeenBefore(...); initialized by main.
    // User include files are always included if forceIncludeCounter > 0.
    // So set to 0 to process each header only once, and 1 to always process.
    // Except that ... code in processFile(...) will increment this variable whenever the string
    // /*mergeCsource:forceIncludeCounter++*/ begins a source line, and will decrement it whenever
    // the string /*mergeCsource:forceIncludeCounter--*/ begins a line.
    int forceIncludeCounter;
    if( doesArgsSetFlag(args,"-includeall") ) {
        if( REPORT_ON_OPTS_SEEN ) fprintf( stderr, "-includeAll present.\n");
        forceIncludeCounter = 1000000;
    } else {
        if( REPORT_ON_OPTS_SEEN ) fprintf( stderr, "-includeAll absent.\n");
        forceIncludeCounter = 0;
    }
    
    // Control whether the filesSeen List is printed before exiting.
    bool printIncludeFilesSeen;
    if( doesArgsSetFlag(args,"-reportFilesSeen") ) {
        if( REPORT_ON_OPTS_SEEN ) fprintf( stderr, "-reportFilesSeen present.\n");
        printIncludeFilesSeen = true;
    } else {
        if( REPORT_ON_OPTS_SEEN ) fprintf( stderr, "-reportFilesSeen absent.\n");
        printIncludeFilesSeen = false;
    }
    
    // Control whether the filesSeen List is printed before exiting.
    bool trackIncludesFlag;
    if( doesArgsSetFlag(args,"-trackIncludesCounter") ) {
        if( REPORT_ON_OPTS_SEEN ) fprintf( stderr, "-trackIncludesCounter present.\n");
        trackIncludesFlag = true;
    } else {
        if( REPORT_ON_OPTS_SEEN ) fprintf( stderr, "-trackIncludesCounter absent.\n");
        trackIncludesFlag = false;
    }
    
    // Usage / help info.
    if( doesArgsSetFlag(args,"-version") || doesArgsSetFlag(args,"--version") || doesArgsSetFlag(args,"-v") || doesArgsSetFlag(args,"--v") ) {
        if( REPORT_ON_OPTS_SEEN ) fprintf( stderr, "-trackIncludesCounter present.\n");
        fprintf( stderr, "This is version 2.0 of mergeCsource, compiled on %s at %s hours.\n", __DATE__, __TIME__ );
        fprintf( stderr, "Using %s.\n", listVersion(args) );
        fprintf( stderr, "Using %s.\n", stringVersion()   );
    }
    
    // Usage / help info.
    if( doesArgsSetFlag(args,"-help") || doesArgsSetFlag(args,"--help") || doesArgsSetFlag(args,"-h") ||  doesArgsSetFlag(args,"-usage") || doesArgsSetFlag(args,"--usage") ) {
        if( REPORT_ON_OPTS_SEEN ) fprintf( stderr, "-trackIncludesCounter present.\n");
        printHelpInfo();
    }
    
    // incDirectories is a list of char*'s. We have to provide our own "chars to GString" function, rather than
    // just passing the GString constructor newStringFromChars(...), because GString.h transforms the latter into
    // a call of _newStringFromChars, which expects a file name and line number as its first arguments rather
    // than just a char *. Hence we define the helper stringFromChars(...) which really does expect just a char *,
    // and passes along the name of *this* file and the line number of the call to newStringFromChars(...) in
    // this file that it wraps. Since args is a List of non-heap char*'s, so is incDirectories; hence its items
    // need not be free'd.
    ListPointer incDirectories = extractAndRemoveIncludeDirectoriesFrom(args);
    if( REPORT_ON_OPTS_SEEN  ) {
        StringPointer idds = listToStringUsingFunction( incDirectories, stringFromChars, "    ", "    \n", "\n" );
        fprintf( stderr, "The following directories will be searched for user include files:\n" );
        fprintf( stderr, idds->string );
        freeString( idds );
    }
    
    // Keep track of the files processed so we can, if requested, not include any files twice.
    // FilesSeen will be an array of GStrings.
    ListPointer headerFilesSeen = newListWithCapacity( UseDefaultList, 25 );
    
    // Loop through the files supplied in argv one by one.
    ListIteratorPtr iterator = newForwardListIterator( args );
    while( listHasNextElement(iterator) ) {

        StringPointer howWeGotHere = newEmptyStringWithCapacity(0);
        StringPointer fileName     = newStringFromChars( (char*) nextListElement(iterator) );

        // Place a distinctive header at the beginning of the contents of this file
        // so it's easy to see where each file listed on the command line begins in the merged file.
        printf( "/* -------------------------------\n" );
        printf( " * ---------- %s\n", fileName->string );
        printf( " * -------------------------------\n" );
        printf( " */\n" );

        processFile( howWeGotHere, fileName, incDirectories, forceIncludeCounter, trackIncludesFlag, headerFilesSeen );
        
        freeString( howWeGotHere );
        freeString( fileName     );
    }
    destroyIterator( iterator );
    
    int idx;
    if( printIncludeFilesSeen ) fprintf( stderr, "\n         Device         I-Node     Seen    Used     H?  path\n" );
    for( idx = 0; idx < lengthOfList(headerFilesSeen); idx ++ ) {
        HeaderFileInfoPtr hfi = getPtrAtIndexInList( idx, headerFilesSeen );
        if( printIncludeFilesSeen ) fprintf( stderr, "     %10d     %10d     %4d    %4d     %c   %s\n", 
                            hfi->deviceNumber, hfi->inodeNumber, hfi->seenCount, hfi->usedCount, (hfi->isDotH?'h':' '), hfi->path->string ); 
        freeString( hfi->path );
        free( hfi );
    }
    destroyList( headerFilesSeen, NULL );
    freeString( lineBuffer             );
    destroyList( args,            NULL );
    destroyList( incDirectories,  NULL );
    
    #if defined(DMALLOC) && false
    
        // When chasing down an unfree'd block of memory, it can be useful to uncomment one of the
        // printMsgAnd...(...) below, set a breakpoint on it, use an interactive debuffer to set the
        // value of dummy to the pointer value for which you'd like to know "where the dickens was
        // this allocated," and then execute the printMsg...(...) (or use the debugger) to find out.
    
        dmalloc_shutdown();
        ListPointer dummy = (ListPointer) 0x9df48;
    
        // lall_printMsgAndListOrigin( "ReplaceWithNameOfPointerBeingQueried", dummy );
        // la_printMsgAndListOrigin( "ReplaceWithNameOfPointerBeingQueried", dummy );
        // ll_printMsgAndListOrigin( "ReplaceWithNameOfPointerBeingQueried", dummy );
    
    #endif
        
    return 0;
}

// --------------------------------------------------------------------------------------
//
//    processFile(...).
//
// --------------------------------------------------------------------------------------

// Process a file looking for non-system #includes (ie #include "...").
// pathToFile is either an absolute path to the file in question,
// or a relative path from the working directory (in which mergeCsource was launched).
// In either case, it includes the name of the file; it may or may not have a path prefix
// (ie contain one or more /'s) because it might just be a file name in the working directory.
// Hmm. We could arrange for one always to be there by prepending "./" if a name on the
// execute line doesn't begin with "/" - would that be cleaner?
void processFile( StringPointer embeddingSequence, StringPointer pathToFile, ListPointer incDirs, int forceIncludeCounter, bool trackIncludesFlag, ListPointer filesSeen ) {
        
    // So, see if there's a path prefix in fileName so we can prepend it to what appears in a #include,
    // which by def'n is always relative to the folder containing the file being scanned.
    // extractPathPrefixIn(...) returns a String of length 0 if no path prefix is found.
    // But a StringPointer is ALWAYS returned.
    StringPointer pathPrefixInFileName = extractPathPrefixIn( pathToFile );
    
    // Update the argument we use to keep track of the sequence of #includes that got us here,
    // which is distinct from the relative path to this file from the working directory 
    // (eg suppose we went through a bunch of files in the same directory - they all have the
    // same location relative to the WD, so that wouldn't change).
    StringPointer includeFileSequenceIncludingMe = newStringFromChars( embeddingSequence->string );
    if( embeddingSequence->length > 0 ) {
        appendCharsToString(  " - ", includeFileSequenceIncludingMe );
    }
    appendCharsToString(  pathToFile->string, includeFileSequenceIncludingMe );
    
    if( ECHO_FILES_INCLUDED ) fprintf( stderr, "Processing %s.\n", includeFileSequenceIncludingMe->string );
    
    FILE * fp = fopen( pathToFile->string, "r" );
    struct stat fileInfo;
    if( fp == NULL ) {
        if( fp == NULL ) {
            fprintf( stderr, "\n     Failed to open the file \"%s\" requested in \"%s\". Aborting execution.\n\n", pathToFile->string, includeFileSequenceIncludingMe->string );
            exit(1);
        }
    } else {
        if( fstat( fileno(fp), &fileInfo ) ) {
            fprintf( stderr, "\n     Unexpected implementation error: fstat(...) failed on %s.\n     Aborting execution.\n\n", pathToFile->string );
            exit(1);
        }
        if( isHeaderFileSeenBefore( &fileInfo, pathToFile, filesSeen, forceIncludeCounter ) ) {
            return;
        }
    }
    
    // Loop through the file line-by-line.
    // If a line begins #include "...", call ourself recursively to replace the #include by the contents of that file.
    // Otherwise, copy the line to stdout.
    enum ReturnedValue status;
    do {
        status = readNextLineFromFileIntoString( fp, lineBuffer );
        if( status == SomethingToDo ) {
            StringPointer fileToInclude = doesLineStartWithLocalInclude( lineBuffer );
            if( fileToInclude != NULL ) {
                // Yeah, this line is a #include "..." - replace it by the contents of the named file.
                // printf( " ******************** Found an include file named \"%s\"!\n", fileToInclude->string );
                StringPointer relPathToNestedFile = newStringFromString( pathPrefixInFileName );
                appendStringToString( fileToInclude, relPathToNestedFile );
                if( stat(relPathToNestedFile->string,&fileInfo) == 0 ) {
                    // The file exists in the same directory.
                    processFile( includeFileSequenceIncludingMe, relPathToNestedFile, incDirs, forceIncludeCounter, trackIncludesFlag, filesSeen );
                } else {
                    // The file does NOT exist in the same directory, so see if we can find it in one of the user-supplied (-I) directories.
                    int idx;
                    for( idx = 0; idx < lengthOfList(incDirs); idx++ ) {
                        shrinkLengthOfStringToInt( relPathToNestedFile, 0 );
                        appendCharsToString( getPtrAtIndexInList(idx,incDirs), relPathToNestedFile );
                        appendCharToString(  '/',                              relPathToNestedFile );
                        appendStringToString( fileToInclude, relPathToNestedFile );
                        if( DEBUG ) fprintf( stderr, "Looking for %s in %s.\n", fileToInclude->string, relPathToNestedFile->string );
                        if( stat(relPathToNestedFile->string,&fileInfo) == 0 ) {
                            // Found it.
                            processFile( includeFileSequenceIncludingMe, relPathToNestedFile, incDirs, forceIncludeCounter, trackIncludesFlag, filesSeen );
                            break;    // We found and processed the file, so exit the for loop.
                        }
                    }
                    if( idx == lengthOfList(incDirs) ) {
                        // Failed to find the requested file.
                        fprintf( stderr, "Failed to find the file %s (%s). Aborting execution.\n", fileToInclude->string, includeFileSequenceIncludingMe->string );
                        exit(1);
                    }
                }
                freeString( fileToInclude       );
                freeString( relPathToNestedFile );
            } else {
                // This line is NOT a #include "...", so copy it on to stdout
                // /*mergeCsource-forceInclusion*/ /*mergeCsource-restoreMergeOnceParameter*/
                puts( lineBuffer->string );
                if( strncmp("/*mergeCsource:forceIncludeCounter++*/",lineBuffer->string,strlen("/*mergeCsource:forceIncludeCounter++*/")) == 0 ) {
                    forceIncludeCounter++;
                    if( trackIncludesFlag ) fprintf( stderr, "mergeCsource:forceIncludeCounter -> %d\n", forceIncludeCounter );
                }
                if( strncmp("/*mergeCsource:forceIncludeCounter--*/",lineBuffer->string,strlen("/*mergeCsource:forceIncludeCounter--*/")) == 0 ) {
                    forceIncludeCounter--;
                    if( trackIncludesFlag ) fprintf( stderr, "mergeCsource:forceIncludeCounter -> %d\n", forceIncludeCounter );
                }
            }
        }
    } while( status == SomethingToDo );
    
    fclose( fp );
    freeString( includeFileSequenceIncludingMe );
    freeString( pathPrefixInFileName           );
}

// --------------------------------------------------------------------------------------
//
//    Helpers.
//
// --------------------------------------------------------------------------------------

bool readNextLineFromFileIntoString( FILE * fp, StringPointer line ) {
    
    shrinkLengthOfStringToInt( line, 0 );
    
    while( true ) {
        
        int nextChar = fgetc( fp );
        
        if( nextChar == '\n' ) {
            // We've reached the end of a line. Don't include it in the string.
            return SomethingToDo;
            
        } else if( nextChar == EOF ) {
            if( line->length > 0 ) {
                // The last line in the file is terminated by the EOF, not by a \n,
                return SomethingToDo;
            } else {
                // At the end of the file & no more content to return.
                return AllDone;
            }
        }
        appendCharToString( nextChar, line );
    }
}

StringPointer extractPathPrefixIn( StringPointer str ) {
    int idx = str->length - 1;
    while( idx >= 0 && str->string[idx] != '/' ) {
        idx --;
    }
    if( idx < 0 ) {
        // Didn't find a '/', so str just contains a file name.
        StringPointer pathPrefix = newEmptyStringWithCapacity(0);
        return pathPrefix;
    } else {
        // str->string[idx] == '/', and it's the last one in str->string.
        // So we build and return a string containing the path and terminating '/',
        // but not the file name.
        int pathLength = idx + 1;
        StringPointer pathPrefix = newEmptyStringWithCapacity( pathLength );
        for( idx = 0; idx < pathLength; idx++ ) {
            appendCharToString( str->string[idx], pathPrefix );
        }
        if( DEBUG ) fprintf( stderr, "Extracted the path prefix \"%s\" from \"%s\".\n", pathPrefix->string, str->string );
        return pathPrefix;
    }
}

#define MatchNextCharOrReturnFalse(CHAR) if( (idx < strLen) && (CHAR == str[idx]) ) { idx++; } else return false

// Returns NULL if the line does NOT begin with the pattern \s*#\s*include\s*"[^"]*";
// if it does, return a String containing the characters found between the double quotes.
// In the latter case, the user is expected to free the String.
StringPointer doesLineStartWithLocalInclude( StringPointer line ) {
    
    int    idx    = 0;
    int    strLen = line->length;
    char * str    = line->string;
    
    // Skip over an initial \s*
    while( idx < strLen && isspace(str[idx]) )
        idx++;
        
    // #
    MatchNextCharOrReturnFalse( '#' );  
    
    // Skip over \s*
    while( idx < strLen && isspace(str[idx]) )
        idx++;

    // Match the seven characters "include". Note that MatchNextCharOrReturnFalse is a cp *macro* (defined above).
    MatchNextCharOrReturnFalse( 'i' );  
    MatchNextCharOrReturnFalse( 'n' );  
    MatchNextCharOrReturnFalse( 'c' );  
    MatchNextCharOrReturnFalse( 'l' );  
    MatchNextCharOrReturnFalse( 'u' );  
    MatchNextCharOrReturnFalse( 'd' );  
    MatchNextCharOrReturnFalse( 'e' );  
    
    // Match \s*
    while( idx < strLen && isspace(str[idx]) )
        idx++;
    
    // Match " (an opening double quote)
    MatchNextCharOrReturnFalse( '"' );  
    
    // Scan up to the next " (the closing double quote) or the end of the line
    StringPointer fileName = newEmptyStringWithCapacity(100);
    while( idx < strLen && (str[idx] != '"') ) {
        appendCharToString( str[idx++], fileName );
    }
    
    if( idx < strLen ) {
        // If the very last character matches, the previous loop terminates with idx == strLen - 1.
        // If we find a closing double quote before the end of the string,  then idx <  strLen - 1.
        return fileName;
    } else {
        // If we run off the end of the string w/o finding a closing double quote, we'll have idx == strLen
        freeString( fileName );
        return NULL;
    }
}

// Build a list of the tokens preceded by a -I.
ListPointer extractAndRemoveIncludeDirectoriesFrom( ListPointer args ) {
    ListPointer answer = newListWithCapacity( UseDefaultList, 5 );
    int         idx    = 0;
    while( idx < lengthOfList(args) ) {                                         // lengthOfList(...) must be called after each iteration!
        char * str = getPtrAtIndexInList( idx, args );
        if( strlen(str) == 2 && strncmp( str, "-I", 2 ) == 0 ) {                // -I is the option name used by cpp/gcc.
            if( lengthOfList(args) - 1 > idx ) {                                // Is the index of the last arg > the idx at which the -I resides?
                removeAndReturnPtrAtIndexFromList(idx,args);                    // Delete "-I" from args - don't free it because it's in the stack, not the heap.
                char * ptr = removeAndReturnPtrAtIndexFromList(idx,args);       // Delete the name of the include directory from args, too.
                addPtrToEndOfList( ptr, answer );                               // And add it to the list of include directories.
                // No need to increment idx because we've deleted both the -I and its matching file name from args
                // idx is now the index of the next token in the argument list (if there is one, of course).
            } else {
                fprintf( stderr, "There's a -I as the last command line argument. Aborting execution.\n" );
                exit( 1 );
            }
        } else {
            idx++;
        }
    }
    return answer;
}

// Looks through args for the token optStr.
// Returns true if optStr is present, false otherwise.
bool doesArgsSetFlag( ListPointer args, char * optStr ) {
    int idx;
    for( idx = 0; idx < lengthOfList(args); idx++ ) {
        char * token = (char*) getPtrAtIndexInList( idx, args );
        if( strcasecmp(optStr,token) == 0 ) {
            removeAndReturnPtrAtIndexFromList( idx, args );
            return true;
        }
    }
    return false;
}

// Looks through args for the token optStr. If present, the following token is returned;
// if it's the last token, an error message is printed and the program exits.
// If optStr is absent then NULL is returned.
char * getArgForOptionIfPresent( ListPointer args, char * optStr ) {
    int idx;
    for( idx = 0; idx < lengthOfList(args); idx++ ) {
        char * token = (char*) getPtrAtIndexInList( idx, args );
        if( strcasecmp(optStr,token) == 0 ){
            removeAndReturnPtrAtIndexFromList( idx, args );
            if( idx == lengthOfList(args) ) {
                fprintf( stderr, "\n\n     %s was the last token on the command line - there's supposed to be an argument!.\n", optStr );
                fprintf( stderr, "     Aborting execution.\n\n" );
                exit(1);
            } else {
                return( removeAndReturnPtrAtIndexFromList( idx, args ) );
            }
        }
    }
    return NULL;
}

// chars should always be a non-NULL pointer to an array of chars. It's declared as a void * because it's passed to
// the List module's listToStringUsingFunction(...), and the prototype must work for lists containing pointers to
// anything at all.
StringPointer stringFromChars( void * chars ) {
    return newStringFromChars( (char*) chars );
}

// Return true iff the specified file (a) is a header file (ie path ends with .h) and (b) we've seen it before.
bool isHeaderFileSeenBefore( const struct stat * fileInfo, StringPointer pathToFile, ListPointer headerFilesSeen, int forceIncludeCounter ) {
    
    int thisFilesDN = fileInfo->st_dev;
    int thisFilesIN = fileInfo->st_ino;
    
    // If there's already an entry for this file, increment it's reference count and return true.
    // Otherwise, add an entry for it with reference count one and return false.
    int idx;
    for( idx = 0; idx < lengthOfList(headerFilesSeen); idx ++ ) {
        HeaderFileInfoPtr hfi = getPtrAtIndexInList( idx, headerFilesSeen );
        if( (thisFilesDN == hfi->deviceNumber) && (thisFilesIN == hfi->inodeNumber) ) {
            // We *have* seen it before, so return true iff it's also a header file.
            hfi->seenCount++;
            return (forceIncludeCounter > 0 ? hfi->usedCount++, false : hfi->isDotH );          
        }
    }
    
    // Didn't find it - make a new entry.
    HeaderFileInfoPtr hfi = (HeaderFileInfoPtr) malloc( sizeof(HeaderFileInfo) );
    hfi->deviceNumber = thisFilesDN;
    hfi->inodeNumber  = thisFilesIN;
    hfi->path         = newStringFromString( pathToFile );
    hfi->usedCount    = 1;
    hfi->seenCount    = 1;
       
    // Is this a header file? That is, does pathToFile end in ".h"?
    int    pLen = pathToFile->length;
    char * path = pathToFile->string;
    hfi->isDotH = (pLen >= 2) && (path[pLen-2] == '.') && (path[pLen-1] == 'h');
       
    addPtrToEndOfList( hfi, headerFilesSeen );
    
    return false;
}

static char * message =
"\n"
"Usage: ./mergeCsource [options] files...\n"
"\n"
"where options may be any or all of the following case-insensitive tokens\n"
"\n"
"     -I pathToDirectory       tells mergeCsource to look in the named directory for user include files it can't find\n"
"                              in the same directory as the file containing the #include (repeatable)\n"
"\n"
"     -includeAll              to cause every include file to be echoed to stdout every time it is seen\n"
"                              rather than just the first time it is seen\n"
"\n"
"     -reportFilesSeen         to generate a table of the files seen on stderr at the end of execution\n"
"\n"
"     -trackIncludesCounter    to write a message to stdout every time a line beginning with either of\n"
"                              /*mergeCsource:forceIncludeCounter++*/ or /*mergeCsource:forceIncludeCounter--*/ is seen\n"
"                              (the forceIncludeCounter is initially 0 unless the -includeAll token is supplied\n"
"                              and all user include files are copied to stdout whenever forceIncludeCounter > 0)\n"
"\n"
"     -usage                   generates this message (as do -h, --help, -usage and --usage)\n"
"\n"
"     -version                 report the versions of mergeCsource, the GString module and the List module that are in use\n"
"                              (as do --version, -v and --v)\n"
"\n"
"and files... is a list of C source files, as for example\n"
"\n"
"     ./mergeCsource -I ~jcbeatty/_headers main.c GString.c ListAll.c DictionaryAll.c HashMap.c ThreadedRedBlackTree.c > asm.c\n"
"\n";


void printHelpInfo( void ) {
    printf( message );
}
