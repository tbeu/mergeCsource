/*
 *  List.h
 *
 *  Created by jcbeatty on 15/05/2009.
 *  Copyright 2009 University of Waterloo. All rights reserved.
 *
 */

// A separate macro name controls inclusion of macros that insert __FILE__ and __LINE__ so that ListAll.c can include
// the routine prototypes a 2nd and 3rd times, having in both cases defined macros that prepend la_ and ll_ to the names,
// by #undefining CS241_LIST_MODULE w/o re-executing these #defines.
#ifndef CS241_LIST_ADDFILEANDNAME
#define CS241_LIST_ADDFILEANDNAME

#define newEmptyList(implementation)                                    _newEmptyList(                    __FILE__, __LINE__, implementation                    ) 
#define newListWithCapacity(implementation,minimum)                     _newListWithCapacity(             __FILE__, __LINE__, implementation, minimum           ) 
#define newListFromFixedArrayOfPointers(implementation,theArray,length) _newListFromFixedArrayOfPointers( __FILE__, __LINE__, implementation, theArray, length  ) 
#define listVersion(theList)                              _listVersion(                                   __FILE__, __LINE__,                 theList       )         
#define insertPtrAtBeginningOfList(ptr,theList)           _insertPtrAtBeginningOfList(                    __FILE__, __LINE__,       ptr,      theList       )         
#define insertPtrIntoListAtIndex(ptr,theList,idx)         _insertPtrIntoListAtIndex(                      __FILE__, __LINE__,       ptr,      theList, idx  )         
#define addPtrToEndOfList(ptr,theList)                    _addPtrToEndOfList(                             __FILE__, __LINE__,       ptr,      theList       )         
#define getPtrAtIndexInList(idx,theList)                  _getPtrAtIndexInList(                           __FILE__, __LINE__,            idx, theList       )         
#define removeAndReturnPtrAtIndexFromList(idx,theList)    _removeAndReturnPtrAtIndexFromList(             __FILE__, __LINE__,            idx, theList       )         
#define replaceAndReturnPtrAtIndexInList(ptr,idx,theList) _replaceAndReturnPtrAtIndexInList(              __FILE__, __LINE__,       ptr, idx, theList       )         
#define capacityOfList(theList)                           _capacityOfList(                                __FILE__, __LINE__,                 theList       )         
#define isListEmpty(theList)                              _isListEmpty(                                   __FILE__, __LINE__,                 theList       )         
#define makeListEmpty(theList,fcn )                       _makeListEmpty(                                 __FILE__, __LINE__,                 theList, fcn  )         
#define destroyList(theList,fcn )                         _destroyList(                                   __FILE__, __LINE__,                 theList, fcn  )         
#define lengthOfList(theList)                             _lengthOfList(                                  __FILE__, __LINE__,                 theList       )         
#define listToStringUsingFunction(theList,fcn, prefix, gap, suffix ) _listToStringUsingFunction(          __FILE__, __LINE__,                 theList, fcn, prefix, gap, suffix  )                                
#define newForwardListIterator(theList)                   _newForwardListIterator(                        __FILE__, __LINE__,                 theList       )         
#define newBackwardListIterator(theList)                  _newBackwardListIterator(                       __FILE__, __LINE__,                 theList       )         
#define nextListElement(theIterator)                      _nextListElement(                               __FILE__, __LINE__,                 theIterator   )         
#define listHasNextElement(theIterator)                   _listHasNextElement(                            __FILE__, __LINE__,                 theIterator   )         
#define destroyIterator(theIterator)                      _destroyIterator(                               __FILE__, __LINE__,                 theIterator   )

#endif    // CS241_LIST_ADDFILEANDNAME

// Arrange for __func__ to be replaced by the name of the enclosing function when possible.
// __func__ is defined in ANSI C99; early versions of gcc provide this functionality in various ways.
// See http://gcc.gnu.org/onlinedocs/gcc-4.1.2/gcc/Function-Names.html for the details.
#ifndef __FUNC__
#define __FUNC__ (char*) __func__
#if __STDC_VERSION__ < 199901L
#    if __GNUC__ >= 2
#        define __func__ __FUNCTION__
#    else
#        define __func__ "[can't say]"
#    endif
#endif
#endif // __FUNC__

// Arrange that the contents of this file only be compiled once,
// even if directly or indirectly #included more than once.
#ifndef CS241_LIST_MODULE
#define CS241_LIST_MODULE

#include <stdbool.h>

/*mergeCsource:forceIncludeCounter--*/     // Used by mergeCsource - don't touch!
#include "GString.h"
/*mergeCsource:forceIncludeCounter++*/     // Used by mergeCsource - don't touch!

// --------------------------------------------------------------------------------------
//
//      The Abstract Data Type "List".
//
//      Two implementations are supplied:
//
//			ListArray.c		- an expandable native C array of pointers.
//			ListLinked.c	- a doubly-linked list of pointers.
//
//      0-origin indexing is used, so the for a list of length n, the entries are numbered
//      from 0 through length - 1.
//
//      REGARDING __FILE__ and __LINE__:
//
//      It is intended that users will use the routine names #define'd in the
//      #ifdef CS241_LIST_ADDFILEANDNAME section above, which insert the macro names
//      __FILE__ and __LINE__ as the first two parameters for all calls. These are
//      defined in ANSI C to be the file name and line number within that file on which
//      the instance of __FILE__ and __LINE__ appear. No error checking is performed on 
//      them, so if you get tricky and try to invoke one of the _... implementation 
//      routines directly you're on your own :-)...
//
//      Thus the user writes, for example, ListPtr list = newEmptyList( UseListArray )
//      and the relevant #define in the #ifdef CS241_LIST_ADDFILEANDNAME section above 
//      causes the preprocessor to transform this first to
//            ListPtr list = _newEmptyList( __FILE__, __LINE__, UseListArray )
//      and then, via its built-in definitions of __FILE__ and __LINE__, to something like
//            ListPtr list = _newEmptyList( "asm.c", 71, UseListArray )
//      That's all there is to it if you're using just ListArray.c or ListLinked.c.
//      Note that __FILE__ and __LINE__ are part of ANSI C - see page 160 (offset 172)
//      of http://www.open-std.org/JTC1/SC22/wg14/www/docs/n1124.pdf and go back at
//      least as far as Kernighan and Ritche ("The C Programming Language" - see 
//      section A 12.10, page 233).
//
//      However, if you're using ListAll.c instead of just ListArray.c or ListLinked.c,
//      ListArray.c itself further #defines _newEmptyList as (for example)
//      la_newEmptyList, so the prototypes below actually becomes something like
//             ListPtr list = la_newEmptyList( "asm.c", 71, UseListArray )
//      This is done twice, once before #including ListArray.c and once before #including
//      ListLinked.c. ListAll.c goes on to provide a definition of _newEmptyList(...) -
//      The user's call of newEmptyList( UseListArray ) then gets transformed as
//      just described to a call of _newEmptyList( __FILE__, __LINE__, UseListArray ),
//      which looks at the imlementation parameter and creates a new list using the
//      requested implementation. Subsequent calls to a list routine invoke code in
//      ListAll.c that passes the call along to the correct la_...(...) or ll_...(...)
//      implementation.
//
// --------------------------------------------------------------------------------------

// ListStruct_ will be *defined* in the implementation - ie in ListArray.c and ListLinked.c.
// The compiler's happy with our use here of ListPointer because it knows how large a pointer is;
// it wouldn't be happy if we said something like sizeof(ListStruct) because it doesn't yet know 
// the sizeof a ListStruct. We define both ListPointer and ListPtr so we don't have to remember
// which is correct :-).
typedef struct ListStruct_ ListStruct, * ListPointer, * ListPtr;

#ifndef CS241_LIST_IMPLEMENTATION_NAMES
#define CS241_LIST_IMPLEMENTATION_NAMES

// We surround the definition of ListImplementationNames & ListImplementation because there's
// no need for them to be different for the two implementations.

// For specifying which List implementation to use / is in use.
// These are needed so that when using ListAll.c so that a user can choose which implementation s/he wants.
enum ListImplementationNames { UseDefaultList, UseListArray, UseListLinked };
typedef enum ListImplementationNames ListImplementation;

#endif  // of CS241_LIST_IMPLEMENTATION_NAMES


// To keep gcc on Solaris happy - for some reason, snprintf(...) is missing from stdio.h.
#ifdef SOLARIS
    int snprintf( char *, size_t, const char *, ... );
#endif

// -----------------------------------------------------------------------------------------------------------------------------
//
//     Constructors
//
// -----------------------------------------------------------------------------------------------------------------------------

/*   Create and return a new, empty List.
** 
**       Pre:  none additional - implementation must be a ListImplementationName.
** 
**       Post: returns a new, empty list. If more than one implementation is included in the program
**             then the implementation requested must be one of those included; otherwise the value
**             of implementation must be either UseUseDefaultList or match the implementation available.
*/

ListPointer _newEmptyList( char * fileName, int lineNumber, ListImplementation implementation );

// -----------------------------------------------------------------------------------------------------------------------------

/*   Create and return a new, empty List.
** 
**        Pre:  none additional - implementation must be a ListImplementationName.
** 
**        Post: returns a new, empty list able to contain at least the specified number of nodes.
**              (This minimum is not-relevant for linked list implementations, for which this constructor
**              is effectively identicall to calling newList() directly.) 
**
**              If more than one implementation is included in the program
**              then the implementation requested must be one of those included; otherwise the value
**              of implementation must be either UseUseDefaultList or match the implementation available.
*/

ListPointer _newListWithCapacity( char * fileName, int lineNumber, ListImplementation implementation, int minimum );

// -----------------------------------------------------------------------------------------------------------------------------

/*   Create and return a List containing the elements supplied in a native C array.
** 
**       Pre:  theArray is not NULL; length ≥ 0 is the number of elements in theArray; 
**             none of the elements in theArray are NULL.
** 
**       Post: returns a new list containing the elements of theArray in the same order.
**
**             If more than one implementation is included in the program,
**             then the implementation requested must be one of those included; otherwise the value
**             of implementation must be either UseUseDefaultList or match the implementation available.
*/

ListPointer _newListFromFixedArrayOfPointers( char * fileName, int lineNumber, ListImplementation implementation, void * theArray[], int length  );

// -----------------------------------------------------------------------------------------------------------------------------
//
//     Public routines.
//
// -----------------------------------------------------------------------------------------------------------------------------

/*   Return a string documenting the version of the List implementation in use.
*/

char * _listVersion( char * fileName, int lineNumber, ListPointer theList );

// -----------------------------------------------------------------------------------------------------------------------------

/*   Insert an element at the beginning of the list, ahead of all present contents.
** 
**       Pre:  ptr != NULL; theList != NULL.
** 
**       Post: the existing contents of the list move right one index position,
**             ptr becomes the entry at index 0, and
**             the list's size is increased by 1.
*/

void _insertPtrAtBeginningOfList( char * fileName, int lineNumber, void * ptr, ListPointer theList );

// -----------------------------------------------------------------------------------------------------------------------------

/*   Insert an element at the specified index, moving the existing entry at that index and all subsequent entries right one position.
 ** 
 **       Pre:  ptr != NULL; theList != NULL; 0 <= idx <= theList->length.
 ** 
 **       Post: the existing contents of the list at indicese [idx,theList->length-1] move right one index position,
 **             ptr becomes the entry at index idx, and
 **             the list's size is increased by 1.
 **
 **   Note that idx is allowed to be as large as the initial size of the List, in which case calling this routine
 **   is equivalent to calling _addPtrToEndOfList(...).
 */

void _insertPtrIntoListAtIndex( char * fileName, int lineNumber, void * ptr, ListPointer theList, int idx );

// -----------------------------------------------------------------------------------------------------------------------------

/*   Append an element to the end of the list, behind all present contents.
** 
**       Pre:  ptr != NULL; theList != NULL.
** 
**       Post: the list's size is increased by 1, with
**             ptr as the last entry.
*/

void _addPtrToEndOfList( char * fileName, int lineNumber, void * ptr, ListPointer theList );

// -----------------------------------------------------------------------------------------------------------------------------

/*   Return the pointer curerntly at index idx in the list.
** 
**       Pre:  theList != NULL.
** 
**       Post: returns the pointer at index idx in the list.
**             The list itself is unchanged.
*/

void *  _getPtrAtIndexInList( char * fileName, int lineNumber, int idx, ListPointer theList );

// -----------------------------------------------------------------------------------------------------------------------------

/*   Return the pointer curerntly at index idx in the list.
** 
**       Pre:  theList != NULL; 0 <= idx < theList->length.
** 
**       Post: returns the pointer at index idx in the list, which is deleted from list.
**             Pointers at positions idx+1 through theList->length move left one index.
**             The size of the list decreases by one.
*/

void * _removeAndReturnPtrAtIndexFromList( char * fileName, int lineNumber, int idx, ListPointer theList );

// -----------------------------------------------------------------------------------------------------------------------------

/*   Replace and return the pointer curerntly at index idx in the list.
** 
**       Pre:  ptr != NULL; theList != NULL; 0 <= idx < theList->length.
** 
**       Post: returns the pointer at index idx in the list, which is deleted from list.
**             The pointer supplied replaces the initial contents of the list at index idx.
**             The size of the list is unchanged.
*/

void * _replaceAndReturnPtrAtIndexInList( char * fileName, int lineNumber, void * ptr, int idx, ListPointer theList );

// -----------------------------------------------------------------------------------------------------------------------------

/*   Returns the capacity of the list.
** 
**      theList != NULL.
** 
**      Post: returns the number of elements for which storage is currently available in the list.
**            Possibly useful for array-backed implementations; not useful for list-backed implements,
**            for which it will always return lengthOf( theList ).
*/

int _capacityOfList( char * fileName, int lineNumber, ListPointer theList );

// -----------------------------------------------------------------------------------------------------------------------------

/*   Reports whether theList is or is not empty.
** 
**      Pre:  theList != NULL.
** 
**      Post: Returns true or false (as defined in <stdbool.h>, depending on whether theList is or is not empty
*/

bool _isListEmpty( char * fileName, int lineNumber, ListPointer theList );

// -----------------------------------------------------------------------------------------------------------------------------

/*   Removes all existing entries from the list.
**   If a user-defined function is supplied, it will be called once on each entry's data pointer.
** 
**       Pre:  theList != NULL.
** 
**       Post: All memory previously allocated by routines of the List implementation has been free'd,
**             except that required to represent an empty list. The user-supplied function has been called 
**             once for each entry with the entry's data pointer as argument.
*/

typedef void FreeDataFcn( void * arg );

void _makeListEmpty( char * fileName, int lineNumber, ListPointer theList, FreeDataFcn * fcn );

// -----------------------------------------------------------------------------------------------------------------------------

/*   Frees all the memory allocated by the List implementation.
**   If a user-defined function is supplied, it will call that once on each data pointer.
** 
**       Pre:  theList != NULL.
**             If fcn != NULL, it will be called once for each node with the node's data pointer as argument.
** 
**       Post: All memory previously allocated by routines of the List implementation has been free'd.
**             The user-supplied function has been called once for each node with the node's data pointer
**             as argument.
*/

void _destroyList( char * fileName, int lineNumber, ListPointer theList, FreeDataFcn * fcn );

// -----------------------------------------------------------------------------------------------------------------------------

/*   Returns the size of the list.
 ** 
 **      theList != NULL.
 ** 
 **      Post: returns the number of elements in the list, some of which may wrap the same pointer w/o
 **            altering the reported size of the list.
 */

int _lengthOfList( char * fileName, int lineNumber, ListPointer theList );

// -----------------------------------------------------------------------------------------------------------------------------

// This typedef defines the identifier genericToStringFunction to be a "pointer to a function( pointer to a void ) 
// that returns a pointer to a String". We can then use this identifier anywhere we could have written 
// "String * (*someFunctionDefinedElsewhere)( void * arg )". We've done that here to make the the signature for 
// "_listToStringUsingFunction(...)" easier to read.
//
// What's this function for? Well, the List functions have no idea what the pointers they manipulate point to,
// so they have no idea how to construct a string representation of whatever you've put into a List. So you,
// the user of a a List, are required to provide a function that when called with one of those pointers as an
// argument will return a String representation of whatever it is you're storing in the List. ListToStringUsingFunction(...)
// will apply the function you supply to each of the pointers in the array, concatenating the strings returned to build
// an overall String representation of whatever's in the List.
//
// Finally, and annoyingly, any actual function you define will need to declare its argument to be a pointer to void,
// even though YOU, and the function, know what it's a pointer to. The compiler will complain about a type mismatch
// if you declare it to be a pointer to whatever it's intended to process. If you're a Java programmer this will seem
// weird, because in Java "void *" corresponds to Object, and whatever your function is designed to accept is, in Java
// terms, a specialization of "void *". Sorry - your intuition is fine, but C has its own ideas :-).
typedef StringPointer ToStringFcn( void * arg );
String * _listToStringUsingFunction( char * fileName, int lineNumber, ListPointer theList, ToStringFcn * fcn, char * prefix, char * gap, char * suffix );                               

// -----------------------------------------------------------------------------------------------------------------------------
//
//     Iterators
//
// -----------------------------------------------------------------------------------------------------------------------------

// ListIteratorStruct is defined by the implementation.
typedef struct ListIteratorStruct_ * ListIteratorPtr;

ListIteratorPtr _newForwardListIterator(  char * fileName, int lineNumber, ListPointer     theList     );
ListIteratorPtr _newBackwardListIterator( char * fileName, int lineNumber, ListPointer     theList     );
void *          _nextListElement(         char * fileName, int lineNumber, ListIteratorPtr theIterator );
bool            _listHasNextElement(      char * fileName, int lineNumber, ListIteratorPtr theIterator );
void            _destroyIterator(         char * fileName, int lineNumber, ListIteratorPtr theIterator );

#endif // of CS241_LIST_MODULE
