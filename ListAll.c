
/*
 *  ListAll.c
 *
 *  Created by jcbeatty on 24/06/2009.
 *  Copyright 2009 University of Waterloo. All rights reserved.
 *
 */

// --------------------------------------------------------------------------------------------
//
// Use the C preprocessor to redefine the interface routine names to prepend each with "la_",
// and then #include ListArray.c. Later on we'll redefine them again to prepend each with "ll_"
// and #include ListLinked.c. After which we'll define the original routine names to dispatch
// to whichever of the two implementations is actually in use, based on the implementation
// field of the ListStruct defined in this file. These macros also change the ListStructs
// defined in ListArray.c and ListLinked.c to la_ListStruct and ll_ListStruct so that the 
// three ListStructs end up with different names.
//
// We generally adopt the common convention of writing
//
//     typedef struct NameOfStruct_ {
//         ...
//     } NameOfStruct, * NameOfStructPtr;
//
// Because of the way in which we are wrapping the interface defined in this file around
// multiple List implementations in such a way as to make it possible for a program to switch
// between this interface and the interface for only one of the implementations, you'll find a
//
//     typedef struct ListStruct_ ListStruct, * ListPointer, * ListPtr;
//
// in List.h (which needs only that ListPointer be defined) and non-identical
//
//     typedef struct ListStruct_ {
//        ...     
//      };
//    
// in ListArray.c and ListLinked.c - non-identical because the *implementations* need slightly
// different fields. For example, ListArray.c needs a ptr to the partially-filled array of
// item pointers, while ListLinked.c needs pointers to the first and last items in the list.
//
// --------------------------------------------------------------------------------------------

// Observe that this provides prototypes for all the ListADT interface routines & defines 
// the typedef ListPointer
#include "List.h"

// Setup for #include ListArray.c
#define  ListStruct_                       la_ListStruct_
#define  ListStruct                        la_ListStruct
#define  ListPointer                       la_ListPointer
#define  ListPtr                           la_ListPtr
#define  FreeDataFcn                       la_FreeDataFcn
#define  ToStringFcn                       la_ToStringFcn
#define  ListIteratorStruct_               la_ListIteratorStruct_
#define  ListIterator                      la_ListIterator
#define  ListIteratorPtr                   la_ListIteratorPtr
#define _newEmptyList                      la_newEmptyList
#define _newListWithCapacity               la_newListWithCapacity
#define _newListFromFixedArrayOfPointers   la_newListFromFixedArrayOfPointers
#define _listVersion                       la_listVersion
#define _insertPtrAtBeginningOfList        la_insertPtrAtBeginningOfList
#define _insertPtrIntoListAtIndex          la_insertPtrIntoListAtIndex
#define _addPtrToEndOfList                 la_addPtrToEndOfList
#define _getPtrAtIndexInList               la_getPtrAtIndexInList
#define _removeAndReturnPtrAtIndexFromList la_removeAndReturnPtrAtIndexFromList
#define _replaceAndReturnPtrAtIndexInList  la_replaceAndReturnPtrAtIndexInList
#define _capacityOfList                    la_capacityOfList
#define _isListEmpty                       la_isListEmpty
#define _makeListEmpty                     la_makeListEmpty
#define _destroyList                       la_destroyList
#define _lengthOfList                      la_lengthOfList
#define _listToStringUsingFunction         la_listToStringUsingFunction
#define _newForwardListIterator            la_newForwardListIterator
#define _newBackwardListIterator           la_newBackwardListIterator
#define _nextListElement                   la_nextListElement
#define _listHasNextElement                la_listHasNextElement
#define _destroyIterator                   la_destroyIterator

// ListArray.c begins with a #include "List.h" - we must #undefine CSC241_LIST_MODULE so that the
// content of List.h is processed again, but with the interface names altered per the above #defines.
#undef CS241_LIST_MODULE

/*mergeCsource:forceIncludeCounter++*/     // Used by mergeCsource - don't touch!
#include "ListArray.c"
/*mergeCsource:forceIncludeCounter--*/     // Used by mergeCsource - don't touch!

// The preprocessor will complain if we just redefine a macro, so first we need to undefine them.
#undef  ListStruct_
#undef  ListStruct
#undef  ListPointer
#undef  ListPtr
#undef  FreeDataFcn
#undef  ToStringFcn
#undef  ListIteratorStruct_
#undef  ListIterator
#undef  ListIteratorPtr
#undef _newEmptyList
#undef _newListWithCapacity
#undef _newListFromFixedArrayOfPointers
#undef _listVersion
#undef _insertPtrAtBeginningOfList
#undef _insertPtrIntoListAtIndex
#undef _addPtrToEndOfList
#undef _getPtrAtIndexInList
#undef _removeAndReturnPtrAtIndexFromList
#undef _replaceAndReturnPtrAtIndexInList
#undef _capacityOfList
#undef _isListEmpty
#undef _makeListEmpty
#undef _destroyList
#undef _lengthOfList
#undef _listToStringUsingFunction
#undef _newForwardListIterator
#undef _newBackwardListIterator
#undef _nextListElement
#undef _listHasNextElement
#undef _destroyIterator

// ListLinked.c begins with a #include "List.h" - we must undefine CSC241_LIST_MODULE so that the
// content of List.h is processed again, but with the interface names altered per the above #defines.
#undef CS241_LIST_MODULE

// Setup for #include ListLinked.c
#define  ListStruct_                       ll_ListStruct_
#define  ListStruct                        ll_ListStruct
#define  ListPointer                       ll_ListPointer
#define  ListPtr                           ll_ListPtr
#define  FreeDataFcn                       ll_FreeDataFcn
#define  ToStringFcn                       ll_ToStringFcn
#define  ListIteratorStruct_               ll_ListIteratorStruct_
#define  ListIterator                      ll_ListIterator
#define  ListIteratorPtr                   ll_ListIteratorPtr
#define _newEmptyList                      ll_newEmptyList
#define _newListWithCapacity               ll_newListWithCapacity
#define _newListFromFixedArrayOfPointers   ll_newListFromFixedArrayOfPointers
#define _listVersion                       ll_listVersion
#define _insertPtrAtBeginningOfList        ll_insertPtrAtBeginningOfList
#define _insertPtrIntoListAtIndex          ll_insertPtrIntoListAtIndex
#define _addPtrToEndOfList                 ll_addPtrToEndOfList
#define _getPtrAtIndexInList               ll_getPtrAtIndexInList
#define _removeAndReturnPtrAtIndexFromList ll_removeAndReturnPtrAtIndexFromList
#define _replaceAndReturnPtrAtIndexInList  ll_replaceAndReturnPtrAtIndexInList
#define _capacityOfList                    ll_capacityOfList
#define _isListEmpty                       ll_isListEmpty
#define _makeListEmpty                     ll_makeListEmpty
#define _destroyList                       ll_destroyList
#define _lengthOfList                      ll_lengthOfList
#define _listToStringUsingFunction         ll_listToStringUsingFunction
#define _newForwardListIterator            ll_newForwardListIterator
#define _newBackwardListIterator           ll_newBackwardListIterator
#define _nextListElement                   ll_nextListElement
#define _listHasNextElement                ll_listHasNextElement
#define _destroyIterator                   ll_destroyIterator

/*mergeCsource:forceIncludeCounter++*/     // Used by mergeCsource - don't touch!
#include "ListLinked.c"
/*mergeCsource:forceIncludeCounter--*/     // Used by mergeCsource - don't touch!

// Undefine the routine names again before actually defining them to dispatch between the two implementations.
#undef  ListStruct_
#undef  ListStruct
#undef  ListPointer
#undef  ListPtr
#undef  FreeDataFcn
#undef  ToStringFcn
#undef  ListIteratorStruct_
#undef  ListIterator
#undef  ListIteratorPtr
#undef _newEmptyList
#undef _newListWithCapacity
#undef _newListFromFixedArrayOfPointers
#undef _listVersion
#undef _insertPtrAtBeginningOfList
#undef _insertPtrIntoListAtIndex
#undef _addPtrToEndOfList
#undef _getPtrAtIndexInList
#undef _removeAndReturnPtrAtIndexFromList
#undef _replaceAndReturnPtrAtIndexInList
#undef _capacityOfList
#undef _isListEmpty
#undef _makeListEmpty
#undef _destroyList
#undef _lengthOfList
#undef _listToStringUsingFunction
#undef _newForwardListIterator
#undef _newBackwardListIterator
#undef _nextListElement
#undef _listHasNextElement
#undef _destroyIterator

// --------------------------------------------------------------------------------------
//
//     Data structures.
//
// --------------------------------------------------------------------------------------

// The "cookie" field is set to a special bit pattern (ie the 64 bits of ASCII for "ListList") by the List constructors;
// the List methods then abort with an error message if they're passed an alleged ListStruct whose cookie field doesn't match.
// It's a union so that we can easily set the cookie to the 8 chars we want (using strVal), and efficiently test a ListStruct's
// cookie with a single integer comparison (using intVal). Note that according to ANSI C, a long long is a least 8 bytes in size.
typedef union { char strVal[8]; unsigned long long intVal; } List_Cookie;

// Define the constant to which the constructors will initialize the cookie field of the ListStruct they create via malloc(...). 
// We use { 'L', 'i', 's', 't', 'L', 'i', 's', 't' } rather than "ListList" to avoid worrying about the 9th character '\0' 
// that implicitly terminates "ListList".
static List_Cookie list_cookieValue = { .strVal = { 'L', 'i', 's', 't', 'L', 'i', 's', 't' } };    // "ListList"

// Observe that it's a pointer to *this* struct that's passed back to users of this "merged" version of the
// List ADT, not a pointer to the struct defined in ListArray.c or ListLinked.c - it's a pointer to one or
// another of those that is stashed in the struct we're defining here, along with the implementation that
// tells us which of those structs we have a pointer to.

struct ListStruct_ {
    List_Cookie                 cookie;               // For verifying that List routines have actually been passed an initialized ListStruct.
    char                      * calledFromFile;       // Name of the file from which the constructor that allocated this ListStruct was called. (Located in the stack.)
    int                         calledFromLine;       // Line number in calledFromFile from which the constructor that allocatd this ListStruct was called.
    ListImplementation          implementation;       // One of UseListArray and UseListLinked.
    union {                                           // The impStructPtr field is *either* a pointer to a ListArray struct *or a pointer to a ListLinked struct.
        struct la_ListStruct_ *     la;               // The implementation field tells us which, and we then use either ->impStructPtr.la or ->impStructPtr.ll,
        struct ll_ListStruct_ *     ll;               // as is appropriate. Note that it's *our* responsibility to do this correctly.
    }                           impStructPtr;
};

static List_Cookie listIter_cookieValue = { .strVal = { 'L', 'i', 's', 't', 'I', 't', 'e', 'r' } };  // "ListIter"

typedef struct ListIteratorStruct_ {
    List_Cookie                 cookie;               // For verifying that ListIter routines have actually been passed an initialized ListIteratorStruct.
    ListImplementation          implementation;       // One of UseListArray and UseListLinked.
    void                      * listIterStructPtr;    // A pointer to a la_ListIteratorStruct or ll_ListIteratorStruct, resp.
} ListIteratorStruct;

// --------------------------------------------------------------------------------------
//
//     Private helpers (definitions are at the end of the file).
//
// --------------------------------------------------------------------------------------

static void lall_crashIfPtrIsNULL( char * fileName, int lineNumber, char * function, void *          ptr, char * msg );
static void lall_crashIfListCookieIsBad( char * fileName, int lineNumber, char * function, ListPointer     ptr             );
static void lall_crashIfIterCookieIsBad( char * fileName, int lineNumber, char * function, ListIteratorPtr ptr             );
// --------------------------------------------------------------------------------------
//
//     The public interface.
//
//     In this file we "dispatch" each called interface routien to the corresponding
//     implementation routine. Depending on which implementation is requested, we call
//     implementation-specific routines whose name begins either "la_..." or "ll_...".
//
// --------------------------------------------------------------------------------------

ListPointer _newEmptyList( char * fileName, int lineNumber, ListImplementation imp ) {
    
    void        * impPointer = NULL;
    ListPointer   result;
    
    switch( imp ) {
            
        case UseDefaultList: 
        case UseListArray:  
            impPointer = la_newEmptyList( fileName, lineNumber, UseListArray  );
            if( impPointer != NULL ) {
                result = (ListPointer) malloc( sizeof(struct ListStruct_) );
                result->cookie.intVal   = list_cookieValue.intVal;
                result->calledFromFile  = fileName;
                result->calledFromLine  = lineNumber;
                result->implementation  = UseListArray;
                result->impStructPtr.la = impPointer;
            }
            return result;
            
        case UseListLinked: 
            impPointer = ll_newEmptyList( fileName, lineNumber, UseListLinked );
            if( impPointer != NULL ) {
                result = (ListPointer) malloc( sizeof(struct ListStruct_) );
                result->cookie.intVal   = list_cookieValue.intVal;
                result->calledFromFile  = fileName;
                result->calledFromLine  = lineNumber;
                result->implementation  = UseListLinked;
                result->impStructPtr.ll = impPointer;
            }
            return result;
            
        default:         
            fprintf( stderr, "ERROR: impossible List implementation in newEmptyList(...) switch called from line %d in file %s.\n", lineNumber, fileName ); exit(1);
    }
}

ListPointer _newListWithCapacity( char * fileName, int lineNumber, ListImplementation imp, int minimum ) {
    
    void        * impPointer = NULL;
    ListPointer   result;
    
    switch( imp ) {
            
        case UseDefaultList: 
        case UseListArray:  
            impPointer = la_newListWithCapacity( fileName, lineNumber, UseListArray, minimum  );
            if( impPointer != NULL ) {
                result = (ListPointer) malloc( sizeof(struct ListStruct_) );
                result->cookie.intVal   = list_cookieValue.intVal;
                result->calledFromFile  = fileName;
                result->calledFromLine  = lineNumber;
                result->implementation  = UseListArray;
                result->impStructPtr.la = impPointer;
            }
            break;
            
        case UseListLinked: 
            impPointer = ll_newListWithCapacity( fileName, lineNumber, UseListLinked, minimum );
            if( impPointer != NULL ) {
                result = (ListPointer) malloc( sizeof(struct ListStruct_) );
                result->cookie.intVal   = list_cookieValue.intVal;
                result->calledFromFile  = fileName;
                result->calledFromLine  = lineNumber;
                result->implementation  = UseListLinked;
                result->impStructPtr.ll = impPointer;
            }
            break;
            
        default:         
            fprintf( stderr, "ERROR: impossible List implementation in newListWithCapacity(...) switch called from line %d in file %s.\n", lineNumber, fileName ); 
            exit(1);
    }
    return result;
}

ListPointer _newListFromFixedArrayOfPointers( char * fileName, int lineNumber, ListImplementation imp, void * theArray[], int length  ) {
    
    void        * impPointer = NULL;
    ListPointer   result;
    
    switch( imp ) {
            
        case UseDefaultList: 
        case UseListArray:  
            impPointer = la_newListFromFixedArrayOfPointers( fileName, lineNumber, UseListArray, theArray, length  );
            if( impPointer != NULL ) {
                result = (ListPointer) malloc( sizeof(struct ListStruct_) );
                result->cookie.intVal = list_cookieValue.intVal;
                result->calledFromFile  = fileName;
                result->calledFromLine  = lineNumber;
                result->implementation = UseListArray;
                result->impStructPtr.la   = impPointer;
            }
            return result;
            
        case UseListLinked: 
            impPointer = ll_newListFromFixedArrayOfPointers( fileName, lineNumber, UseListLinked, theArray, length );
            if( impPointer != NULL ) {
                result = (ListPointer) malloc( sizeof(struct ListStruct_) );
                result->cookie.intVal = list_cookieValue.intVal;
                result->calledFromFile  = fileName;
                result->calledFromLine  = lineNumber;
                result->implementation  = UseListLinked;
                result->impStructPtr.ll = impPointer;
            }
            return result;
            
        default:         
            fprintf( stderr, "ERROR: impossible List implementation in newListFromFixedArrayOfPointers(...) switch called from line %d in file %s.\n", lineNumber, fileName ); exit(1);
    }
}

#define versionLIstrLen 200
static char lall_versionLI[versionLIstrLen];

char * _listVersion( char * fileName, int lineNumber, ListPointer theList ) {
    
    lall_crashIfPtrIsNULL(       fileName, lineNumber, __FUNC__, theList, "list pointer" );
    lall_crashIfListCookieIsBad( fileName, lineNumber, __FUNC__, theList                 );
    
    // If you change versionLIstrLen, take care that it's long enough to hold what's written into it by snprintf(...),
    // an example of which is the xx character string "??? (compiled on Jun 24 2009 at 05:34:24 hours)\0".
    // The C preprocessor will replace __DATE__ and __TIME__ with appropriate literal strings.
    
    // First we construct a string documenting the version of this file (ListAll.c).
    #ifdef DMALLOC
        char * dmallocFlag = "w/dmalloc ";
    #else
        char * dmallocFlag = "";
    #endif
    snprintf( lall_versionLI, versionLIstrLen, "ListAll version 2.0 %s(compiled on %s at %s hours) + ", dmallocFlag, __DATE__, __TIME__ );
    
    // Next we obtain the version of the implementation being used for this list.
    char * iv; 
    switch( theList->implementation ) {
        case UseListArray:  iv = la_listVersion( fileName, lineNumber, theList->impStructPtr.la ); break;
        case UseListLinked: iv = ll_listVersion( fileName, lineNumber, theList->impStructPtr.ll ); break;
        default:            fprintf( stderr, "ERROR: impossible List implementation in listVersion(...) switch called from line %d in file %s.\n", lineNumber, fileName ); exit(1);
    }
    
    // We want to concatenative the two versions. But because we want to avoid allocating dynamic memory,
    // which we'd have to warn users to free if they are being anal retentive about memory leaks, we're
    // constructing our reply in a fixed size statically allocated chunk of memory. As a result, if the
    // concatenation would require more space than we've reserved, we need to ensure that we stop copying
    // when we reach the end of the space reserved. And because strncpy won't have appended a nul if it
    // runs out of space, in that circumstance we have to add it manually. (In what follows, the -1 is
    // to ensure there's space for that nul.)
    if( strlen(lall_versionLI) + strlen(iv) < versionLIstrLen - 1 ) {
        // There's enouch space for both strings AND a nul.
        return strncat( lall_versionLI, iv, versionLIstrLen - strlen(lall_versionLI) );
    } else {
        strncat( lall_versionLI, iv, versionLIstrLen - strlen(lall_versionLI) );
        lall_versionLI[ versionLIstrLen - 1 ] = '\0';
        return lall_versionLI;
    }
}

#undef versionLIstrLen

void _insertPtrAtBeginningOfList( char * fileName, int lineNumber, void * ptr, ListPointer theList ) {
    lall_crashIfPtrIsNULL(       fileName, lineNumber, __FUNC__, ptr,     "item pointer" );
    lall_crashIfPtrIsNULL(       fileName, lineNumber, __FUNC__, theList, "list pointer" );
    lall_crashIfListCookieIsBad( fileName, lineNumber, __FUNC__, theList                 );
    switch( theList->implementation ) {
        case UseListArray:  la_insertPtrAtBeginningOfList( fileName, lineNumber, ptr, theList->impStructPtr.la ); return;
        case UseListLinked: ll_insertPtrAtBeginningOfList( fileName, lineNumber, ptr, theList->impStructPtr.ll ); return;
        default:            fprintf( stderr, "ERROR: impossible List implementation in insertPtrAtBeginningOfList(...) switch called from line %d in file %s.\n", lineNumber, fileName ); exit(1);
    } 
}

void _insertPtrIntoListAtIndex( char * fileName, int lineNumber, void * ptr, ListPointer theList, int idx ) {
    lall_crashIfPtrIsNULL(       fileName, lineNumber, __FUNC__, ptr,     "item pointer" );
    lall_crashIfPtrIsNULL(       fileName, lineNumber, __FUNC__, theList, "list pointer" );
    lall_crashIfListCookieIsBad( fileName, lineNumber, __FUNC__, theList                 );
    switch( theList->implementation ) {
        case UseListArray:  la_insertPtrIntoListAtIndex( fileName, lineNumber, ptr, theList->impStructPtr.la, idx ); return;
        case UseListLinked: ll_insertPtrIntoListAtIndex( fileName, lineNumber, ptr, theList->impStructPtr.ll, idx ); return;
        default:            fprintf( stderr, "ERROR: impossible List implementation in insertPtrIntoListAtIndex(...) switch called from line %d in file %s.\n", lineNumber, fileName ); exit(1);
    }
}

void _addPtrToEndOfList( char * fileName, int lineNumber, void * ptr, ListPointer theList ) {
    lall_crashIfPtrIsNULL(       fileName, lineNumber, __FUNC__, ptr,     "item pointer" );
    lall_crashIfPtrIsNULL(       fileName, lineNumber, __FUNC__, theList, "list pointer" );
    lall_crashIfListCookieIsBad( fileName, lineNumber, __FUNC__, theList                 );
    switch( theList->implementation ) {
        case UseListArray:  la_addPtrToEndOfList( fileName, lineNumber, ptr, theList->impStructPtr.la ); return;
        case UseListLinked: ll_addPtrToEndOfList( fileName, lineNumber, ptr, theList->impStructPtr.ll ); return;
        default:            fprintf( stderr, "ERROR: impossible List implementation in addPtrToEndOfList(...) switch called from line %d in file %s.\n", lineNumber, fileName ); exit(1);
    }
}

void * _getPtrAtIndexInList( char * fileName, int lineNumber, int idx, ListPointer theList ) {
    lall_crashIfPtrIsNULL(       fileName, lineNumber, __FUNC__, theList, "list pointer" );
    lall_crashIfListCookieIsBad( fileName, lineNumber, __FUNC__, theList                 );
    switch( theList->implementation ) {
        case UseListArray:  return la_getPtrAtIndexInList( fileName, lineNumber, idx, theList->impStructPtr.la  );
        case UseListLinked: return ll_getPtrAtIndexInList( fileName, lineNumber, idx, theList->impStructPtr.ll );
        default:            fprintf( stderr, "ERROR: impossible List implementation in getPtrAtIndexInList(...) switch called from line %d in file %s.\n", lineNumber, fileName ); exit(1);
    }
}

void * _removeAndReturnPtrAtIndexFromList( char * fileName, int lineNumber, int idx, ListPointer theList ) {
    lall_crashIfPtrIsNULL(       fileName, lineNumber, __FUNC__, theList, "list pointer" );
    lall_crashIfListCookieIsBad( fileName, lineNumber, __FUNC__, theList                 );
    switch( theList->implementation ) {
        case UseListArray:  return la_removeAndReturnPtrAtIndexFromList( fileName, lineNumber, idx, theList->impStructPtr.la  );
        case UseListLinked: return ll_removeAndReturnPtrAtIndexFromList( fileName, lineNumber, idx, theList->impStructPtr.ll );
        default:            fprintf( stderr, "ERROR: impossible List implementation in removeAndReturnPtrAtIndexFromList(...) switch called from line %d in file %s.\n", lineNumber, fileName ); exit(1);
    }
}

void * _replaceAndReturnPtrAtIndexInList( char * fileName, int lineNumber, void * ptr, int idx, ListPointer theList ) {
    lall_crashIfPtrIsNULL(       fileName, lineNumber, __FUNC__, ptr,     "item pointer" );
    lall_crashIfPtrIsNULL(       fileName, lineNumber, __FUNC__, theList, "list pointer" );
    lall_crashIfListCookieIsBad( fileName, lineNumber, __FUNC__, theList                 );
    switch( theList->implementation ) {
        case UseListArray:  return la_replaceAndReturnPtrAtIndexInList( fileName, lineNumber, ptr, idx, theList->impStructPtr.la );
        case UseListLinked: return ll_replaceAndReturnPtrAtIndexInList( fileName, lineNumber, ptr, idx, theList->impStructPtr.ll );
        default:            fprintf( stderr, "ERROR: impossible List implementation in replaceAndReturnPtrAtIndexInList(...) switch called from line %d in file %s.\n", lineNumber, fileName ); exit(1);
    }
}

int _capacityOfList( char * fileName, int lineNumber, ListPointer theList ) {
    lall_crashIfPtrIsNULL(       fileName, lineNumber, __FUNC__, theList, "list pointer" );
    lall_crashIfListCookieIsBad( fileName, lineNumber, __FUNC__, theList                 );
    switch( theList->implementation ) {
        case UseListArray:  return la_capacityOfList( fileName, lineNumber, theList->impStructPtr.la );
        case UseListLinked: return ll_capacityOfList( fileName, lineNumber, theList->impStructPtr.ll );
        default:         fprintf( stderr, "ERROR: impossible List implementation in capacityOfList(...) switch called from line %d in file %s.\n", lineNumber, fileName ); exit(1);
    }
}

bool _isListEmpty( char * fileName, int lineNumber, ListPointer theList ) {
    lall_crashIfPtrIsNULL(       fileName, lineNumber, __FUNC__, theList, "list pointer" );
    lall_crashIfListCookieIsBad( fileName, lineNumber, __FUNC__, theList                 );
    switch( theList->implementation ) {
        case UseListArray:  return la_isListEmpty( fileName, lineNumber, theList->impStructPtr.la );
        case UseListLinked: return ll_isListEmpty( fileName, lineNumber, theList->impStructPtr.ll );
        default:            fprintf( stderr, "ERROR: impossible List implementation in isListEmpty(...) switch called from line %d in file %s.\n", lineNumber, fileName ); exit(1);
    }
}

void _makeListEmpty( char * fileName, int lineNumber, ListPointer theList, FreeDataFcn * fcn ) {
    lall_crashIfPtrIsNULL(       fileName, lineNumber, __FUNC__, theList, "list pointer" );
    lall_crashIfListCookieIsBad( fileName, lineNumber, __FUNC__, theList );
    switch( theList->implementation ) {
        case UseListArray:  la_makeListEmpty( fileName, lineNumber, theList->impStructPtr.la, fcn ); return;
        case UseListLinked: ll_makeListEmpty( fileName, lineNumber, theList->impStructPtr.ll, fcn ); return;
        default:            fprintf( stderr, "ERROR: impossible List implementation in makeListEmpty(...) switch called from line %d in file %s.\n", lineNumber, fileName ); exit(1);
    }
}

void _destroyList( char * fileName, int lineNumber, ListPointer theList, FreeDataFcn * fcn ) {

    lall_crashIfPtrIsNULL(       fileName, lineNumber, __FUNC__, theList, "list pointer" );
    lall_crashIfListCookieIsBad( fileName, lineNumber, __FUNC__, theList                 );
    
    switch( theList->implementation ) {
            
        case UseListArray:  
            la_destroyList( fileName, lineNumber, theList->impStructPtr.la, fcn );
            free( theList );
            return;
            
        case UseListLinked: 
            ll_destroyList( fileName, lineNumber, theList->impStructPtr.ll, fcn );
            free( theList );
            return;
            
        default:        
            fprintf( stderr, "ERROR: impossible List implementation in destroyList(...) switch called from line %d in file %s.\n", lineNumber, fileName ); 
            exit(1);
    }
}

int _lengthOfList( char * fileName, int lineNumber, ListPointer theList ) {
    lall_crashIfPtrIsNULL(       fileName, lineNumber, __FUNC__, theList, "list pointer" );
    lall_crashIfListCookieIsBad( fileName, lineNumber, __FUNC__, theList                 );
    switch( theList->implementation ) {
        case UseListArray:  return la_lengthOfList( fileName, lineNumber, theList->impStructPtr.la );
        case UseListLinked: return ll_lengthOfList( fileName, lineNumber, theList->impStructPtr.ll );
        default:            fprintf( stderr, "ERROR: impossible List implementation in lengthOfList(...) switch called from line %d in file %s.\n", lineNumber, fileName ); exit(1);
    }
}

String * _listToStringUsingFunction( char * fileName, int lineNumber, ListPointer theList, ToStringFcn * fcn, char * prefix, char * gap, char * suffix ) {
    lall_crashIfPtrIsNULL(       fileName, lineNumber, __FUNC__, theList, "list pointer" );
    lall_crashIfListCookieIsBad( fileName, lineNumber, __FUNC__, theList                 );
    switch( theList->implementation ) {
        case UseListArray:  return la_listToStringUsingFunction( fileName, lineNumber, theList->impStructPtr.la, fcn, prefix, gap, suffix );
        case UseListLinked: return ll_listToStringUsingFunction( fileName, lineNumber, theList->impStructPtr.ll, fcn, prefix, gap, suffix );
        default:            fprintf( stderr, "ERROR: impossible List implementation in listToStringUsingFunction(...) switch called from line %d in file %s.\n", lineNumber, fileName ); exit(1);
    }
}

// -----------------------------------------------------------------------------------------------------------------------------
//
//     Iterators
//
// -----------------------------------------------------------------------------------------------------------------------------

ListIteratorPtr _newForwardListIterator( char * fileName, int lineNumber, ListPointer theList ) {
    
    void               * iterPointer = NULL;
    ListIteratorStruct * result;
    
    lall_crashIfPtrIsNULL( fileName, lineNumber, __FUNC__, theList, "list pointer" );
    
    switch( theList->implementation ) {
            
        case UseListArray:  
            iterPointer = la_newForwardListIterator( fileName, lineNumber, theList->impStructPtr.la );
            if( iterPointer != NULL ) {
                result = (ListIteratorPtr) malloc( sizeof(struct ListIteratorStruct_) );
                result->cookie.intVal     = listIter_cookieValue.intVal;
                result->implementation    = UseListArray;
                result->listIterStructPtr = iterPointer;
            }
            return result;
            
        case UseDefaultList: 
        case UseListLinked: 
            iterPointer = ll_newForwardListIterator( fileName, lineNumber, theList->impStructPtr.ll );
            if( iterPointer != NULL ) {
                result = (ListIteratorPtr) malloc( sizeof(struct ListIteratorStruct_) );
                result->cookie.intVal   = listIter_cookieValue.intVal;
                result->implementation    = UseListLinked;
                result->listIterStructPtr = iterPointer;
            }
            return result;
            
        default:         
            fprintf( stderr, "ERROR: impossible List implementation in newForwardListIterator(...) switch called from line %d in file %s.\n", lineNumber, fileName ); exit(1);
    }
}

ListIteratorPtr _newBackwardListIterator( char * fileName, int lineNumber, ListPointer theList ) {
    
    void               * iterPointer = NULL;
    ListIteratorStruct * result;

    lall_crashIfPtrIsNULL( fileName, lineNumber, __FUNC__, theList, "list pointer" );
    
    switch( theList->implementation ) {
            
        case UseListArray:  
            iterPointer = la_newBackwardListIterator( fileName, lineNumber, theList->impStructPtr.la );
            if( iterPointer != NULL ) {
                result = (ListIteratorPtr) malloc( sizeof(struct ListIteratorStruct_) );
                result->cookie.intVal     = listIter_cookieValue.intVal;
                result->implementation    = UseListArray;
                result->listIterStructPtr = iterPointer;
            }
            return result;
            
        case UseDefaultList: 
        case UseListLinked: 
            iterPointer = ll_newBackwardListIterator( fileName, lineNumber, theList->impStructPtr.ll );
            if( iterPointer != NULL ) {
                result = (ListIteratorPtr) malloc( sizeof(struct ListIteratorStruct_) );
                result->cookie.intVal     = listIter_cookieValue.intVal;
                result->implementation    = UseListLinked;
                result->listIterStructPtr = iterPointer;
            }
            return result;
            
        default:         
            fprintf( stderr, "ERROR: impossible List implementation in newBackwardListIterator(...) switch called from line %d in file %s.\n", lineNumber, fileName ); exit(1);
    }
}

void * _nextListElement( char * fileName, int lineNumber, ListIteratorPtr theIterator ) {
    lall_crashIfPtrIsNULL( fileName, lineNumber, __FUNC__, theIterator, "list iterator pointer" );
    lall_crashIfIterCookieIsBad( fileName, lineNumber, __FUNC__, theIterator                          );
    switch( theIterator->implementation ) {
        case UseListArray:  return la_nextListElement( fileName, lineNumber, theIterator->listIterStructPtr );
        case UseListLinked: return ll_nextListElement( fileName, lineNumber, theIterator->listIterStructPtr );
        default:            fprintf( stderr, "ERROR: impossible List implementation in nextListElement(...) switch called from line %d in file %s.\n", lineNumber, fileName ); exit(1);
    }
}

bool _listHasNextElement( char * fileName, int lineNumber, ListIteratorPtr theIterator ) {
    lall_crashIfPtrIsNULL(       fileName, lineNumber, __FUNC__, theIterator, "list iterator pointer" );
    lall_crashIfIterCookieIsBad( fileName, lineNumber, __FUNC__, theIterator );
    switch( theIterator->implementation ) {
        case UseListArray:  return la_listHasNextElement( fileName, lineNumber, theIterator->listIterStructPtr );
        case UseListLinked: return ll_listHasNextElement( fileName, lineNumber, theIterator->listIterStructPtr );
        default:            fprintf( stderr, "ERROR: impossible List implementation in listHasNextElement(...) switch called from line %d in file %s.\n", lineNumber, fileName ); exit(1);
    }
}

void _destroyIterator( char * fileName, int lineNumber, ListIteratorPtr theIterator ) {

    lall_crashIfPtrIsNULL(       fileName, lineNumber, __FUNC__, theIterator, "list iterator pointer" );
    lall_crashIfIterCookieIsBad( fileName, lineNumber, __FUNC__, theIterator                          );

    switch( theIterator->implementation ) {
            
        case UseListArray:
            la_destroyIterator( fileName, lineNumber, theIterator->listIterStructPtr );
            free( theIterator );
            return;
            
        case UseListLinked: 
            ll_destroyIterator( fileName, lineNumber, theIterator->listIterStructPtr );
            free( theIterator );
            return;
            
        default:         
            fprintf( stderr, "ERROR: impossible List implementation in destroyIterator(...) switch called from line %d in file %s.\n", lineNumber, fileName ); 
            exit(1);
    }
}

// For debugging purposes - in particular, for chasing down an unfree'd block of heap memory that you know to be a ListStruct,
// but for which you want to know *which* ListStruct. The expectation is that you've got the address of the block, assign that
// to a dummy ListPointer with a cast, then call this routine (or use an interactive debugger) to answe that questions.
// Of course, if you hand it the wrong address you may crash the program...
void lall_printMsgAndListOrigin( char * msg, ListPointer theList ) {
    fprintf( stderr, "%s: called from line %d in file %s.\n", msg, theList->calledFromLine, theList->calledFromFile );
}

// --------------------------------------------------------------------------------------
//
//     Private helpers.
//
// --------------------------------------------------------------------------------------

static void lall_crashIfPtrIsNULL( char * fileName, int lineNumber, char * function, void * ptr, char * msg ) {
    if( ptr != NULL ) {
        return;
    } else {
        fprintf( stderr, "ERROR: the %s you passed to %s on line %d of file %s is NULL.\n", msg, function, lineNumber, fileName );
        exit(1);
    }
}

static   // Used to verify that the cookie with which initialized ListStructs are tagged is correct.
void lall_crashIfListCookieIsBad( char * fileName, int lineNumber, char * function, ListPointer ptr ) {
    if( ptr->cookie.intVal != list_cookieValue.intVal ) {
        fprintf( stderr, "ERROR: the List pointer passed to %s on line %d of the file %s has a bad cookie. Aborting execution.\n", function, lineNumber, fileName );
        exit(2);
    }
}

static   // Used to verify that the cookie with which initialized ListStructs are tagged is correct.
void lall_crashIfIterCookieIsBad( char * fileName, int lineNumber, char * function, ListIteratorPtr ptr ) {
    if( ptr->cookie.intVal != listIter_cookieValue.intVal ) {
        fprintf( stderr, "ERROR: the List Iterator pointer passed to %s on line %d of the file %s has a bad cookie. Aborting execution.\n", function, lineNumber, fileName );
        exit(2);
    }
}
