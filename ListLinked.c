/*
 *  ListLinked.c
 *
 *   A doubly-linked list wrapping pointers to whatever (whatever != NULL).
 *
 *  Created by jcbeatty.
 *  Copyright 2009 University of Waterloo. All rights reserved.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "List.h"

// For monitoring the heap. Must *follow* stdio and stdlib.
#ifdef DMALLOC
/*mergeCsource:forceIncludeCounter--*/     // Used by mergeCsource - don't touch!
#include "dmallocFor241.h"
/*mergeCsource:forceIncludeCounter++*/     // Used by mergeCsource - don't touch!
#endif

#define DEBUG_LIST_LINKED false

// --------------------------------------------------------------------------------------
//
//    Data structures.
//
// --------------------------------------------------------------------------------------

struct NodeStruct_ {
    struct NodeStruct_ * prev;        // NULL for the first node in the list.
    struct NodeStruct_ * next;        // NULL for the last  node in the list.
    void               * dPtr;        // Address of a datum in the list; may not be NULL.
};

typedef struct NodeStruct_ Node, * NodePtr;

// The "cookie" field is set to a special bit pattern (ie the 64 bits of ASCII for "ListLink") by the ListLinked constructors;
// the ListLinked methods then abort with an error message if they're passed an alleged ListStruct whose cookie field doesn't match.
// It's a union so that we can easily set the cookie to the 8 chars we want (using strVal), and efficiently test a ListStruct's
// cookie with a single integer comparison (using intVal). Note that according to ANSI C, a long long is a least 8 bytes in size.
typedef union { char strVal[8]; long long intVal; } LL_Cookie;                                 // ListLinked_Cookie

// The constructor _newEmptyList(...), which is called by the other constructors, will initialize this.
static LL_Cookie ll_CookieValue = { .strVal = { 'L', 'i', 's', 't', 'L', 'i', 'n', 'k' } };    // "ListLink"

struct ListStruct_ {
    LL_Cookie            cookie;            // For verifying that ListLinked routines have actually been passed an initialized ListStruct.
    char               * calledFromFile;    // Name of the file from which the constructor that allocated this ListStruct was called. (Located in the stack.)
    int                  calledFromLine;    // Line number in calledFromFile from which the constructor that allocatd this ListStruct was called.
    int                  length;            // Number of nodes in the list (ie if two nodes wrap the same data, they both count).
    struct NodeStruct_ * firstNode;         // Address of the first node in the list (the "front" of the list)
    struct NodeStruct_ * lastNode;          // Address of the last  node in the list (the "back"  of the list)
};

// -------------------------------------------------

typedef enum { LLI_FORWARD, LLI_REVERSE } LLI_Direction;

static LL_Cookie ll_Iter_cookieValue = { .strVal = { 'L', 'L', '_', 'I', 't', 'e', 'r', 'C' } };    // "LL_IterC"

struct ListIteratorStruct_ {
    LL_Cookie            cookie;       // For verifying that ListIterator routines have actually been passed an initialized ListIteratorStruct.
    LLI_Direction        direction;
    struct ListStruct_ * list;
    struct NodeStruct_ * currentNode;
};

typedef struct ListIteratorStruct_ ListIterator;

// --------------------------------------------------------------------------------------
//
//   Prototypes for helpers (definitions located at the end of the file).
//
// --------------------------------------------------------------------------------------

static void    ll_crashIfIndexNotInRange(       char * fileName, int lineNumber, int idx,             ListPointer theList, char * msg      );
static void    ll_crashIfPtrIsNULL(             char * fileName, int lineNumber,          void * ptr,                      char * msg      );
static void    ll_crashIfListLinkedCookieIsBad( char * fileName, int lineNumber,                      ListPointer theList, char * msg      );
static void    ll_crashIfListIterCookieIsBad(   char * fileName, int lineNumber,                      ListIteratorPtr ptr, char * function );
static NodePtr ll_nodeAtIndexOfList( int idx, ListPointer theList );

// --------------------------------------------------------------------------------------
//
//    Constructors.
//
// --------------------------------------------------------------------------------------

ListPointer _newEmptyList( char * fileName, int lineNumber, ListImplementation implementation ) {
    
    // It's an error to request an implementation that's not available.
    if( implementation != UseDefaultList && implementation != UseListLinked ) {
        fprintf( stderr, "ERROR in call to newEmptyList(...) on line %d of file %s.\n", lineNumber, fileName      );
        fprintf( stderr, "You have requested a List implementation that is not available.  Aborting execution.\n" );
        fprintf( stderr, "(Try using UseDefaultList or UseListLinked instead.)\n"                                 );
        exit(1);
    } 

    ListPointer list = (ListPointer) malloc( sizeof(struct ListStruct_) );
    list->cookie.intVal  = ll_CookieValue.intVal;
    list->calledFromFile = fileName;
    list->calledFromLine = lineNumber;
    list->length         = 0;
    list->firstNode      = NULL;
    list->lastNode       = NULL;
    return list;
}

ListPointer _newListWithCapacity( char * fileName, int lineNumber, ListImplementation implementation, int sizeWanted ) {
    return _newEmptyList( fileName, lineNumber, implementation );
}

ListPointer _newListFromFixedArrayOfPointers( char * fileName, int lineNumber, ListImplementation implementation, void * cArray[], int length )
{
    int idx;
    ll_crashIfPtrIsNULL( fileName, lineNumber, cArray, "newListFromFixedArrayOfPointers' C array pointer" );
    ListPointer theNewList = _newEmptyList( fileName, lineNumber, implementation );
    for( idx = 0; idx < length; idx++ ) {
        _addPtrToEndOfList( fileName, lineNumber, cArray[idx], theNewList );
    }
    return theNewList;
}

// --------------------------------------------------------------------------------------
//
//    Public routines.
//
// --------------------------------------------------------------------------------------

#define ll_versionstrLen 100
static char ll_version[ll_versionstrLen];

char * _listVersion( char * fileName, int lineNumber, ListPointer theList ) {
    // We don't do error checks on theList because we don't use it - it's included for consistency.
    // If you change ll_versionstrLen, take care that it's long enough to hold what's written into it by snprintf(...),
    // an example of which is the 70 character string "List (Linked) version 1.1 (compiled on Jun 24 2009 at 05:37:32 hours)\0".
    // The C preprocessor will replace __DATE__ and __TIME__ with appropriate literal strings.
    #ifdef DMALLOC
        char * dmallocFlag = "w/dmalloc ";
    #else
        char * dmallocFlag = "";
    #endif
    snprintf( ll_version, ll_versionstrLen, "List (Linked) version 2.0 %s(compiled on %s at %s hours)", dmallocFlag, __DATE__, __TIME__ );
    return ll_version;
}

#undef ll_versionstrLen

void _insertPtrAtBeginningOfList( char * fileName, int lineNumber, void * thePtr, ListPointer theList )
{
    ll_crashIfPtrIsNULL(             fileName, lineNumber, thePtr,  "insertPtrAtBeginningOfList's element pointer"                                  );
    ll_crashIfPtrIsNULL(             fileName, lineNumber, theList, "insertPtrAtBeginningOfList's list pointer"                                     );
    ll_crashIfListLinkedCookieIsBad( fileName, lineNumber, theList, "the List pointer passed to _insertPtrAtBeginningOfList(...) has a bad cookie." );
    NodePtr newNode = (NodePtr) malloc( sizeof(struct NodeStruct_) );
    newNode->dPtr   = thePtr;
    if( theList->length == 0 ) {
        newNode->prev      = NULL;
        newNode->next      = NULL;
        theList->firstNode = newNode;
        theList->lastNode  = newNode;
    } else {
        newNode->prev            = NULL;
        newNode->next            = theList->firstNode;
        theList->firstNode->prev = newNode;
        theList->firstNode       = newNode;
    }
    theList->length++;
    return;
}

void _insertPtrIntoListAtIndex( char * fileName, int lineNumber, void * ptr, ListPointer theList, int index ) {
    ll_crashIfPtrIsNULL(             fileName, lineNumber, ptr,     "insertPtrIntoListAtIndex's element pointer"                                 );
    ll_crashIfPtrIsNULL(             fileName, lineNumber, theList, "insertPtrIntoListAtIndex's list parameter"                                  );
    ll_crashIfListLinkedCookieIsBad( fileName, lineNumber, theList, "the List pointer passed to insertPtrIntoListAtIndex(...) has a bad cookie." );
    if( index == 0 ) {
        return _insertPtrAtBeginningOfList( fileName, lineNumber, ptr, theList );
    } else if( index == theList->length ) {
        return _addPtrToEndOfList( fileName, lineNumber, ptr, theList );
    } else {
        ll_crashIfIndexNotInRange( fileName, lineNumber, index, theList, "function insertPtrIntoListAtIndex" );
        // There's at least one node to both the left and right of where the new node goes.
        NodePtr newNode = (NodePtr) malloc( sizeof(struct NodeStruct_) );
        NodePtr nodeToInsertBefore = ll_nodeAtIndexOfList( index, theList );
        NodePtr nodeToInsertAfter  = nodeToInsertBefore->prev;
        newNode->next              = nodeToInsertBefore;
        newNode->prev              = nodeToInsertAfter;
        newNode->dPtr              = ptr;
        nodeToInsertAfter->next    = newNode;
        nodeToInsertBefore->prev   = newNode;
        theList->length++;
        return;
    }
}

void _addPtrToEndOfList( char * fileName, int lineNumber, void * thePtr, ListPointer theList )
{
    ll_crashIfPtrIsNULL(             fileName, lineNumber, thePtr,  "addPtrToEndOfList's element parameter"                               );
    ll_crashIfPtrIsNULL(             fileName, lineNumber, theList, "addPtrToEndOfList's list parameter"                                  );
    ll_crashIfListLinkedCookieIsBad( fileName, lineNumber, theList, "the List pointer passed to addPtrToEndOfList(...) has a bad cookie." );
    NodePtr newNode = (NodePtr) malloc( sizeof(struct NodeStruct_) );
    newNode->dPtr   = thePtr;
    if( theList->length == 0 ) {
        newNode->prev      = NULL;
        newNode->next      = NULL;
        theList->firstNode = newNode;
        theList->lastNode  = newNode;
    } else {
        newNode->prev           = theList->lastNode;
        newNode->next           = NULL;
        theList->lastNode->next = newNode;
        theList->lastNode       = newNode;
    }
    theList->length++;
    return;
}

void *  _getPtrAtIndexInList( char * fileName, int lineNumber, int idx, ListPointer theList )
{
    ll_crashIfPtrIsNULL(             fileName, lineNumber,      theList, "getPtrAtIndexInList's list parameter"                                  );
    ll_crashIfIndexNotInRange(       fileName, lineNumber, idx, theList, "function getPtrAtIndexInList"                                          );
    ll_crashIfListLinkedCookieIsBad( fileName, lineNumber,      theList, "the List pointer passed to getPtrAtIndexInList(...) has a bad cookie." );
    NodePtr node = ll_nodeAtIndexOfList( idx, theList );
    return node->dPtr;  
}

void * _removeAndReturnPtrAtIndexFromList( char * fileName, int lineNumber, int index, ListPointer theList )
{
    ll_crashIfPtrIsNULL(             fileName, lineNumber,        theList, "removeAndReturnPtrAtIndexFromList's list parameter"                                  );
    ll_crashIfIndexNotInRange(       fileName, lineNumber, index, theList, "function removeAndReturnPtrAtIndexFromList"                                          );
    ll_crashIfListLinkedCookieIsBad( fileName, lineNumber,        theList, "the List pointer passed to removeAndReturnPtrAtIndexFromList(...) has a bad cookie." );
    NodePtr node;
    if( index == 0 ) {
        // Deleting the first node on the list.
        node = theList->firstNode;
        if( theList->length == 1 ) {
            // We're left with an empty list. theList->length will be decremented just before returning.
            theList->firstNode = NULL;
            theList->lastNode  = NULL;
        } else {
            // There are at least two nodes, so node->next != NULL.
            theList->firstNode = node->next;
            node->next->prev   = NULL;
        }
    } else if( index == theList->length-1 ) {
        // Deleting the last node on the list.
        // At this point we know that index > 0, so lastNode->prev != NULL.
        node               = theList->lastNode;
        theList->lastNode  = node->prev;
        node->prev->next   = NULL;
    } else {
        // Deleting an interior node from a list containing at least 3 elements.
        node = ll_nodeAtIndexOfList( index, theList );
        node->prev->next = node->next;
        node->next->prev = node->prev;
    }
    void * ptr = node->dPtr;
    free( node );
    theList->length--;
    return ptr;
}

void * _replaceAndReturnPtrAtIndexInList( char * fileName, int lineNumber, void * ptr, int index, ListPointer theList )
{   
    ll_crashIfPtrIsNULL(             fileName, lineNumber, ptr,            "replaceAndReturnPtrAtIndexInList's element parameter"                               );
    ll_crashIfPtrIsNULL(             fileName, lineNumber,        theList, "replaceAndReturnPtrAtIndexInList's list parameter"                                  );
    ll_crashIfListLinkedCookieIsBad( fileName, lineNumber,        theList, "the List pointer passed to replaceAndReturnPtrAtIndexInList(...) has a bad cookie." );
    ll_crashIfIndexNotInRange(       fileName, lineNumber, index, theList, "function removeAndReturnPtrAtIndexFromList"                                         );
    NodePtr node = ll_nodeAtIndexOfList( index, theList );
    void * answer = node->dPtr;
    node->dPtr = ptr;
    return answer;
}

int _capacityOfList( char * fileName, int lineNumber, ListPointer theList ) {
    ll_crashIfPtrIsNULL(             fileName, lineNumber, theList, "capacityOfList"                                                   );
    ll_crashIfListLinkedCookieIsBad( fileName, lineNumber, theList, "the List pointer passed to capacityOfList(...) has a bad cookie." );
    return theList->length;
}

int _lengthOfList( char * fileName, int lineNumber, ListPointer theList ) {
    ll_crashIfPtrIsNULL(             fileName, lineNumber, theList, "lengthOfList"                                                   );
    ll_crashIfListLinkedCookieIsBad( fileName, lineNumber, theList, "the List pointer passed to lengthOfList(...) has a bad cookie." );
    return theList->length;
}

bool _isListEmpty( char * fileName, int lineNumber, ListPointer theList ) {
    ll_crashIfPtrIsNULL(             fileName, lineNumber, theList, "isListEmpty"                                                   );
    ll_crashIfListLinkedCookieIsBad( fileName, lineNumber, theList, "the List pointer passed to isListEmpty(...) has a bad cookie." );
    return theList->length == 0;
}

void _makeListEmpty( char * fileName, int lineNumber, ListPointer theList, FreeDataFcn * fcn ) {
    ll_crashIfPtrIsNULL(             fileName, lineNumber, theList, "makeListEmpty"                                                   );
    ll_crashIfListLinkedCookieIsBad( fileName, lineNumber, theList, "the List pointer passed to makeListEmpty(...) has a bad cookie." );
    NodePtr curr = theList->firstNode;
    NodePtr next = NULL;
    while( curr != NULL ) {
        next = curr->next;
        if( fcn != NULL ) fcn( curr->dPtr );  // Call user function to free heap memory for user's data (including the dPtr).
        free( curr );
        curr = next;
    }
    theList->length    = 0;
    theList->firstNode = NULL;
    theList->lastNode  = NULL;
    return;
}

void _destroyList( char * fileName, int lineNumber, ListPointer theList, FreeDataFcn * fcn ) {
    ll_crashIfPtrIsNULL(             fileName, lineNumber, theList, "destroyList"                                                   );
    ll_crashIfListLinkedCookieIsBad( fileName, lineNumber, theList, "the List pointer passed to destroyList(...) has a bad cookie." );
    _makeListEmpty( fileName, lineNumber, theList, fcn );
    free( theList );
}

String * _listToStringUsingFunction( char * fileName, int lineNumber, ListPointer theList, ToStringFcn * toStringFcn, char * prefix, char * gap, char * suffix )
{
    ll_crashIfPtrIsNULL(             fileName, lineNumber, theList, "destroyList's list parameter is NULL"                                        );
    ll_crashIfListLinkedCookieIsBad( fileName, lineNumber, theList, "the List pointer passed to listToStringUsingFunction(...) has a bad cookie." );
    char   * curGap = "";
    String * answer = newEmptyStringWithCapacity( 100 );
    if( prefix != NULL )
        appendCharsToString( prefix, answer );
    ListIteratorPtr iterator = _newForwardListIterator( fileName, lineNumber, theList );
    while( _listHasNextElement(fileName,lineNumber,iterator) ) {
        void * nextEltPtr = _nextListElement(fileName,lineNumber,iterator);
        if(  gap != NULL )
            appendCharsToString( curGap, answer );
        if( toStringFcn == NULL ) {
            appendCharsToString( nextEltPtr, answer );
        } else {
            String * piece = toStringFcn( nextEltPtr );
            appendStringToString( piece, answer );
            freeString( piece );
        }
        curGap = gap;
    }
    if( suffix != NULL )
        appendCharsToString( suffix, answer );
    free( iterator );
    return answer;
}

// For debugging purposes - in particular, for chasing down an unfree'd block of heap memory that you know to be a ListStruct,
// but for which you want to know *which* ListStruct. The expectation is that you've got the address of the block, assign that
// to a dummy ListPointer with a cast, then call this routine (or use an interactive debugger) to answe that questions.
// Of course, if you hand it the wrong address you may crash the program...
void ll_printMsgAndListOrigin( char * msg, ListPointer theList ) {
    fprintf( stderr, "%s: called from line %d in file %s.\n", msg, theList->calledFromLine, theList->calledFromFile );
}

// --------------------------------------------------------------------------------------
//
//    Iterators.
//
// --------------------------------------------------------------------------------------

ListIteratorPtr _newForwardListIterator( char * fileName, int lineNumber, ListPointer theList ) {
    ll_crashIfPtrIsNULL(             fileName, lineNumber, theList, "newForwardListIterator's list parameter is NULL"                          );
    ll_crashIfListLinkedCookieIsBad( fileName, lineNumber, theList, "the List pointer passed to newForwardListIterator(...) has a bad cookie." );
    ListIteratorPtr newIterator = (ListIteratorPtr) malloc( sizeof(ListIterator) );
    newIterator->cookie.intVal  = ll_Iter_cookieValue.intVal;
    newIterator->direction      = LLI_FORWARD;
    newIterator->list           = theList;
    newIterator->currentNode    = theList->firstNode;
    return newIterator;
}

ListIteratorPtr _newBackwardListIterator( char * fileName, int lineNumber, ListPointer theList ) {
    ll_crashIfPtrIsNULL(             fileName, lineNumber, theList, "newBackwardListIterator's list parameter is NULL"                         );
    ll_crashIfListLinkedCookieIsBad( fileName, lineNumber, theList, "the List pointer passed to newForwardListIterator(...) has a bad cookie." );
    ListIteratorPtr newIterator = (ListIteratorPtr) malloc( sizeof(ListIterator) );
    newIterator->cookie.intVal  = ll_Iter_cookieValue.intVal;
    newIterator->direction      = LLI_REVERSE;
    newIterator->list           = theList;
    newIterator->currentNode    = theList->lastNode;
    return newIterator;
}

bool _listHasNextElement( char * fileName, int lineNumber, ListIteratorPtr theIterator ) {
    ll_crashIfPtrIsNULL(           fileName, lineNumber, theIterator, "listHasNextElement's iterator parameter is NULL" );
    ll_crashIfListIterCookieIsBad( fileName, lineNumber, theIterator, __FUNC__                                          );
    return theIterator->currentNode != NULL;
}

void * _nextListElement( char * fileName, int lineNumber, ListIteratorPtr theIterator ) {
    NodePtr nextNode;
    ll_crashIfPtrIsNULL(           fileName, lineNumber, theIterator, "nextListElement's iterator parameter is NULL" );
    ll_crashIfListIterCookieIsBad( fileName, lineNumber, theIterator, __FUNC__                                       );
    nextNode = theIterator->currentNode;
    if(        theIterator->direction == LLI_FORWARD ) {
        if( theIterator->currentNode != NULL ) theIterator->currentNode = theIterator->currentNode->next;
        return nextNode->dPtr;
    } else if( theIterator->direction == LLI_REVERSE ) {
        if( theIterator->currentNode != NULL ) theIterator->currentNode = theIterator->currentNode->prev;
        return nextNode->dPtr;
    } else {
        fprintf( stderr, "ERROR in nextListElement(...), called from line %d of file %s.\n", lineNumber, fileName     );
        fprintf( stderr, "The direction for this list is neither FORWARD or BACKWARD, which shouldn't ever happen!\n" );
        exit(1);
    }
/*
    // According to the debugger, when executed this code jumps from assigning dir to the default case, 
    // even though dir gets assigned and has the value FORWARD. A compiler bug???
    Direction dir = theIterator->direction;
    switch( dir ) {
        FORWARD:
            if( theIterator->currentNode != NULL ) theIterator->currentNode = theIterator->currentNode->next;
            return answer;
        REVERSE:
            if( theIterator->currentNode != NULL ) theIterator->currentNode = theIterator->currentNode->prev;
            return answer;
        default:
            fprintf( stderr, "ERROR: fatal implementation error in nextListElement - direction not FORWARD or BACKWARD!\n" );
            exit(1);
    }
*/
}

void _destroyIterator( char * fileName, int lineNumber, ListIteratorPtr theIterator ) {
    ll_crashIfPtrIsNULL( fileName, lineNumber, theIterator, "destroyIterator's iterator parameter is NULL" );
    ll_crashIfListIterCookieIsBad( fileName, lineNumber, theIterator, __FUNC__ );
    free( theIterator );
    return;
}

// --------------------------------------------------------------------------------------
//
//    Helpers.
//
// --------------------------------------------------------------------------------------

static
void ll_crashIfIndexNotInRange( char * fileName, int lineNumber, int idx, ListPointer theList, char * msg ) {
    if( (0 <= idx) && (idx < theList->length) ) {
        return;
    } else {
        fprintf( stderr, "ERROR: out-of-range index %d on line %d in file %s (%s). Aborting.\n", idx, lineNumber, fileName, msg );
        exit(1);
    }
}

static    // Accessible only from other routines in this file.
void ll_crashIfPtrIsNULL( char * fileName, int lineNumber, void * ptr, char * msg ) {
    if( ptr != NULL ) {
        return;
    } else {
        fprintf( stderr, "ERROR: attempt to insert a NULL pointer into the list on line %d of file %s (%s).\n", lineNumber, fileName, msg );
        exit(1);
    }
}

static
NodePtr ll_nodeAtIndexOfList( int idx, ListPointer theList ) {
    // Assumes theList != NULL and 0 <= idx < theList->length.
    // Should check to see which end is closer and count from that. Oh well...
    int ctr = 0;
    NodePtr cn = theList->firstNode;
    while( ctr < idx ) {
        ctr++;
        cn = cn->next;
    }
    return cn;
}

static   // Used to verify that the magic cookie with which initialized ListStructs are tagged is correct.
void ll_crashIfListLinkedCookieIsBad( char * fileName, int lineNumber, ListPointer ptr, char * msg ) {
    if( ptr->cookie.intVal != ll_CookieValue.intVal ) {
        fprintf( stderr, "ERROR on line %d of file %s: %s\n", lineNumber, fileName, msg );
        exit(2);
    }
}

static   // Used to verify that the cookie with which initialized ListIteratorStructs are tagged is correct.
void ll_crashIfListIterCookieIsBad( char * fileName, int lineNumber, ListIteratorPtr ptr, char * function ) {
    if( ptr->cookie.intVal != ll_Iter_cookieValue.intVal ) {
        fprintf( stderr, "ERROR: the ListLinked ListIterator pointer passed to %s on line %d of the file %s has a bad cookie. Aborting execution.\n", function, lineNumber, fileName );
        exit(2);
    }
}

#undef DEBUG_LIST_LINKED
