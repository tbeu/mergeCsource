Sources:
<https://www.student.cs.uwaterloo.ca/~cs241/cLibs/mergeCsource>

Notes:
mergeCSource gives incorrect output if compiled with GCC 4.7. It is recommended to use
older GCC (e.g. 4.4) here.

Usage:
./mergeCsource endian.c inflate.c io.c read_data.c snprintf.c mat.c mat4.c mat5.c mat73.c matvar_cell.c matvar_struct.c > ModelicaMatio.c
